/* global TIMEOUT, assert */

describe('Binding', function () {
  // Get Dependencies
  var Bindings, Binding, $;
  before(function (done) {
    this.timeout(TIMEOUT);
    require("../setup");
    requirejs(['Bindings', 'Binding', 'jquery'], function (_Bindings, _Binding, _$) {
      Bindings = _Bindings;
      Binding = _Binding;
      $ = _$;
      done();
    });
  });
  
  describe('#equals', function () {
    it('should equal an exact reference', function () {
      assert.ok(Bindings.CHAOS.equals(Bindings.CHAOS), 'Identical references are equal');
    });
    
    it('should not equal a different Binding', function () {
      assert.ok(!Bindings.WATER.equals(Bindings.CHAOS), 'Different references are not equal');
    });
    
    it('should not equal a new Binding instance configured identically', function () {
      var sameClass = "class";
      var sameOp = ">";
      var b1 = new Binding(sameClass, sameOp);
      var b2 = new Binding(sameClass, sameOp);
      assert.ok(!b1.equals(b2), 'Identical references are equal');
    });
  });
});