/* global TIMEOUT, assert */

describe('Glyph', function () {
  // Get Dependencies
  var Glyphs, Glyph, $;
  before(function (done) {
    this.timeout(TIMEOUT);
    require("../setup");
    requirejs(['Glyphs', 'Glyph', 'jquery'], function (_Glyphs, _Glyph, _$) {
      Glyphs = _Glyphs;
      Glyph = _Glyph;
      $ = _$;
      done();
    });
  });

  describe('#equals', function () {
    it('should be reflective', function () {
      assert.ok(Glyphs.AT.equals(Glyphs.AT), 'A Glyph should equal itself');
    });
    it('should not work on redundant instances', function () {
      var glyph1 = new Glyph("A");
      var glyph2 = new Glyph("A");
      assert.ok(!glyph1.equals(glyph2), 'Multiple instances of the same Glyph should not equal');
    });
    it('should know the difference between Glyphs', function () {
      assert.ok(!Glyphs.AT.equals(Glyphs.GU), 'AT and GU do not equal');
      assert.ok(!Glyphs.DOE.equals(Glyphs.REE), 'DOE and REE do not equal');
      assert.ok(!Glyphs.TA.equals(Glyphs.LO), 'TA and LO do not equal');
      assert.ok(!Glyphs.NA.equals(Glyphs.SHA), 'NA and SHA do not equal');
    });
  });

  describe('#html', function () {
    it('should deduce to its logical symbol', function () {
      var logicalSymbol = Glyphs.AT.character;
      var glyphVal = $(Glyphs.AT.html()).text().trim();
      assert.equal(logicalSymbol, glyphVal, 'The text() of an html call should be the logical operator');
    });
  });
});