/* global TIMEOUT, assert */

describe('Bindings', function () {
  // Get Dependencies
  var Bindings, $;
  before(function (done) {
    this.timeout(TIMEOUT);
    require("../setup");
    requirejs(['Bindings', 'jquery'], function (_Bindings, _$) {
      Bindings = _Bindings;
      $ = _$;
      done();
    });
  });
  
  describe('#values', function () {
    it('should contain all glyphs', function () {
      var values = Bindings.values();
      assert.ok(_.contains(values, Bindings.FIRE), 'Values should contain FIRE');
      assert.ok(_.contains(values, Bindings.WATER), 'Values should contain WATER');
      assert.ok(_.contains(values, Bindings.AIR), 'Values should contain AIR');
      assert.ok(_.contains(values, Bindings.EARTH), 'Values should contain EARTH');
      assert.ok(_.contains(values, Bindings.LIFE), 'Values should contain LIFE');
      assert.ok(_.contains(values, Bindings.CHAOS), 'Values should contain CHAOS');
    });
    
    it('should return a fresh copy each time', function() {
      // Get the normal state
      var values = Bindings.values();
      var unmodifiedLength = values.length;
      assert.equal(values.length, unmodifiedLength, 'Make sure we stored the length correctly');
      
      // Modify the array and assert it has been changed
      values.pop();
      values.pop();
      values.pop();
      assert.equal(values.length, unmodifiedLength - 3, 'Make sure we have 3 fewer than original');
      
      // Try it on a brand new call
      assert.equal(Bindings.values().length, unmodifiedLength, 'New call should be at original length');
    });
  });

  describe('#fromCharacter', function () {
    it('should return undefined for an invalid character', function() {
      assert.equal(undefined, Bindings.fromCharacter('?'), '? is not a character for any Binding');
    });
    
    it('should return the Binding for the corresponding character', function() {
      assert.equal(Bindings.FIRE, Bindings.fromCharacter('|'), '"|" should yield FIRE');
      assert.equal(Bindings.WATER, Bindings.fromCharacter('&'), '"&" should yield WATER');
      assert.equal(Bindings.AIR, Bindings.fromCharacter('>'), '">" should yield AIR');
      assert.equal(Bindings.EARTH, Bindings.fromCharacter('%'), '"%" should yield EARTH');
      assert.equal(Bindings.LIFE, Bindings.fromCharacter('#'), '"#" should yield LIFE');
      assert.equal(Bindings.CHAOS, Bindings.fromCharacter('~'), '"~" should yield CHAOS');
    });
  });
});