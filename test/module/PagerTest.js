/* global TIMEOUT, assert, sinon */

describe('Pager', function () {
  // Get Dependencies
  var Pager, $;
  before(function (done) {
    this.timeout(TIMEOUT);
    require('../setup');
    requirejs(['Pager', 'jquery'], function (_Pager, _$) {
      Pager = _Pager;
      $ = _$;
      $('body').append('<div id="PagerTests"></div>');
      done();
    });
  });

  var ARRAY_SIZE_10 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  var $prev, $next, $display, displayImpl;

  beforeEach(function () {
    var $base = $('#PagerTests');
    $base.empty();

    $prev = $('<div id="PrevControl"></div>');
    $next = $('<div id="NextControl"></div>');
    $display = $('<div id="Display"></div>');
    
    displayImpl = function(list) {
      $display.html(list);
    };
    
    $base.append($prev).append($next).append($display);
  });

  describe('new()', function () {
    it('should set the current page to 1', function () {
      var pager = new Pager(ARRAY_SIZE_10, 2, displayImpl, $prev, $next);
      assert.equal(1, pager.currentPage, 'Should be set to 1');
    });
    it('should store a live copy of the list', function () {
      var list = [1, 2, 3];
      var pager = new Pager(list, 2, displayImpl, $prev, $next);
      assert.equal(3, pager.list.length, 'List starts with 3 elements');
      list.push(4);
      list.push(5);
      assert.equal(5, pager.list.length, 'List ends with 5 elements after two pushs');
    });
  });

  describe('#contents', function () {
    var ARRAY_SIZE_10 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    it('should return the expected contents for each page', function () {
      var pager = new Pager(ARRAY_SIZE_10, 3, displayImpl, $prev, $next);
      // Page 1
      assert.equal(1, pager.contents()[0], 'Page 1 item 1');
      assert.equal(2, pager.contents()[1], 'Page 1 item 2');
      assert.equal(3, pager.contents()[2], 'Page 1 item 3');

      // Page 2
      pager.next();
      assert.equal(4, pager.contents()[0], 'Page 2 item 1');
      assert.equal(5, pager.contents()[1], 'Page 2 item 2');
      assert.equal(6, pager.contents()[2], 'Page 2 item 3');

      // Page 3
      pager.next();
      assert.equal(7, pager.contents()[0], 'Page 3 item 1');
      assert.equal(8, pager.contents()[1], 'Page 3 item 2');
      assert.equal(9, pager.contents()[2], 'Page 3 item 3');

      // Page 4
      pager.next();
      assert.equal(10, pager.contents()[0], 'Page 4 item 1');
    });
  });

  describe('#display', function () {
    it('should call the display logic with the correct contents', function () {
      var pager = new Pager(ARRAY_SIZE_10, 3, displayImpl, $prev, $next);
      pager.next();
      pager.display();
      assert.equal('456', $display.html(), 'The display functionality should print out the second page');
      assert.ok(!$prev.hasClass('hidden'), 'Prev should be enabled');
      assert.ok(!$next.hasClass('hidden'), 'Next should be enabled');
    });
  });

  describe('#next', function () {
    it('should increment the page counter when not on the last page', function () {
      var pager = new Pager(ARRAY_SIZE_10, 5, displayImpl, $prev, $next);
      assert.equal(1, pager.currentPage, 'The initial page is 1');
      pager.next();
      assert.equal(2, pager.currentPage, 'The next page is 2');
    });
    it('should not increment the page counter when on the last page', function () {
      var pager = new Pager(ARRAY_SIZE_10, 10, displayImpl, $prev, $next);
      assert.equal(1, pager.currentPage, 'The initial page is 1');
      pager.next();
      assert.equal(1, pager.currentPage, 'The page should still be 1');
    });
    it('should return the contents of the current page', function () {
      var pager = new Pager(ARRAY_SIZE_10, 1, displayImpl, $prev, $next);
      assert.equal(2, pager.next()[0], 'The second page returns a single item');
    });
  });

  describe('#nextAndDisplay', function () {
    it('should call the display logic with the correct contents', function () {
      var pager = new Pager(ARRAY_SIZE_10, 3, displayImpl, $prev, $next);
      pager.nextAndDisplay();
      assert.equal('456', $display.html(), 'The display functionality should print out the second page');
      assert.ok(!$prev.hasClass('hidden'), 'Prev should be enabled');
      assert.ok(!$next.hasClass('hidden'), 'Next should be enabled');
    });
  });

  describe('#prev', function () {
    it('should not decrement the page counter when on the first page', function () {
      var pager = new Pager(ARRAY_SIZE_10, 5, displayImpl, $prev, $next);
      assert.equal(1, pager.currentPage, 'The initial page is 1');
      pager.prev();
      assert.equal(1, pager.currentPage, 'The page should still be 1');
    });
    it('should decrement the page counter when not on the first page', function () {
      var pager = new Pager(ARRAY_SIZE_10, 5, displayImpl, $prev, $next);
      pager.next();
      assert.equal(2, pager.currentPage, 'Start with page 2');
      pager.prev();
      assert.equal(1, pager.currentPage, 'The previous page is 1');
    });
    it('should return the contents of the current page', function () {
      var pager = new Pager(ARRAY_SIZE_10, 1, displayImpl, $prev, $next);
      pager.next();
      pager.next();
      assert.equal(2, pager.prev()[0], 'The second page returns a single item');
    });
  });

  describe('#prevAndDisplay', function () {
    it('should call the display logic with the correct contents', function () {
      var pager = new Pager(ARRAY_SIZE_10, 3, displayImpl, $prev, $next);
      pager.next();
      pager.prevAndDisplay();
      assert.equal('123', $display.html(), 'The display functionality should print out the first page');
      assert.ok($prev.hasClass('hidden'), 'Prev should be disabled');
      assert.ok(!$next.hasClass('hidden'), 'Next should be enabled');
    });
  });

  describe('#isFirst', function () {
    it('should return true when on the first page', function () {
      var pager = new Pager(ARRAY_SIZE_10, 5, displayImpl, $prev, $next);
      assert.ok(pager.isFirst(), 'The pager is on the first page');
    });
    it('should return false when not on the first page', function () {
      var pager = new Pager(ARRAY_SIZE_10, 5, displayImpl, $prev, $next);
      pager.next();
      assert.ok(!pager.isFirst(), 'The pager is on the first page');
    });
  });

  describe('#isLast', function () {
    it('should return false when not on the last page', function () {
      var pager = new Pager(ARRAY_SIZE_10, 5, displayImpl, $prev, $next);
      assert.ok(!pager.isLast(), 'The pager is on the first page');
    });
    it('should return true when on the last page', function () {
      var pager = new Pager(ARRAY_SIZE_10, 5, displayImpl, $prev, $next);
      pager.next();
      assert.ok(pager.isLast(), 'The pager is on the first page');
    });
  });

  describe('#updateControls', function () {
    it('should update the classes of prev and next controls as pager moves', function () {
      var pager = new Pager(ARRAY_SIZE_10, 3, displayImpl, $prev, $next);
      pager.updateControls();
      assert.ok($prev.hasClass('hidden'), 'Page 1: Prev should be disabled');
      assert.ok(!$next.hasClass('hidden'), 'Page 1: Next should be enabled');
      pager.next();
      pager.updateControls();
      assert.ok(!$prev.hasClass('hidden'), 'Page 2: Prev should be enabled');
      assert.ok(!$next.hasClass('hidden'), 'Page 2: Next should be enabled');
      pager.next();
      pager.updateControls();
      assert.ok(!$prev.hasClass('hidden'), 'Page 3: Prev should be enabled');
      assert.ok(!$next.hasClass('hidden'), 'Page 3: Next should be enabled');
      pager.next();
      pager.updateControls();
      assert.ok(!$prev.hasClass('hidden'), 'Page 4: Prev should be enabled');
      assert.ok($next.hasClass('hidden'), 'Page 4: Next should be disabled');
    });
  });
});