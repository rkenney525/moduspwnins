/* global TIMEOUT, assert */

describe('Rune', function () {
  // Get Dependencies
  var Rune, Bindings, Glyphs;
  before(function (done) {
    this.timeout(TIMEOUT);
    require("../setup");
    requirejs(['Rune', 'Bindings', 'Glyphs'], function (_Rune, _Bindings, _Glyphs) {
      Rune = _Rune;
      Bindings = _Bindings;
      Glyphs = _Glyphs;
      done();
    });
  });

  describe('#equals', function () {
    it('should equal itself', function () {
      var rune = Rune.fromString("A");
      assert.ok(rune.equals(rune), "A Fact should equal itself");
    });
    it('should equal another instance instantiated identically', function () {
      var rune1 = Rune.fromString('B');
      var rune2 = Rune.fromString('B');
      assert.ok(rune1.equals(rune2), 'Two identical instatiations should equal');
    });
    it('should not equal on a partial match', function () {
      var rune1 = Rune.fromString('A');
      var rune2 = Rune.fromString('(A#B)');
      assert.ok(!rune1.equals(rune2), 'Equality is not containment');
    });
    it('should not equal something completely different', function () {
      var rune1 = Rune.fromString('(C|D)');
      var rune2 = Rune.fromString('~(F)');
      assert.ok(!rune1.equals(rune2), 'Clearly different facts should not equal');
    });
  });

  describe('#toString', function () {
    function genericTest(factString) {
      var rune = Rune.fromString(factString);
      var result = rune.toString();
      assert.equal(result, factString, result + ' should be ' + factString);
    }

    it('should yield a String that, when parsed, produces the original Rune', function () {
      var rune = Rune.fromString("((~(A)|B)#~((C&(D>E))))");
      var reflect = Rune.fromString(rune.toString());
      assert.ok(rune.equals(reflect), "The two should be identical");
    });

    it('should return a parsable String representation of the Rune', function () {
      genericTest('A');
      genericTest('(A>B)');
      genericTest('~((A|C))');
      genericTest('(A|(B|(C&~(D))))');
      genericTest('((((A|B)>C)&D)|E)');
    });
  });

  describe('#fromString', function () {
    it('should be able to store a single Glyph', function () {
      var rune = Rune.fromString('A');
      assert.equal(rune.arg0, Glyphs.fromCharacter('A'), 'Verify the A is stored');
      assert.equal(rune.arg1, undefined, 'Verify there is no second argument');
      assert.equal(rune.op, undefined, 'Verify there is no binding');
    });
    it('should be able to store two Glyphs and a Binding', function () {
      var conditional = Rune.fromString('(A>B)');
      var shouldBeAT = conditional.arg0.arg0;
      var shouldBeNA = conditional.arg1.arg0;
      assert.equal(shouldBeAT, Glyphs.fromCharacter('A'), 'Verify the A in A > B');
      assert.equal(shouldBeNA, Glyphs.fromCharacter('B'), 'Verify the B in A > B');
      assert.equal(conditional.op, Bindings.AIR, 'Verify the > in A > B');
    });
    it('should be able to store two Runes and a Binding', function () {
      var rune = '~A > (B # (C & ~D))';
      var complex = Rune.fromString('(~(A)>(B#(C&~(D))))');
      // Check the operator
      assert.equal(complex.op, Bindings.AIR, 'Verify the > in ' + rune);

      // Check the first argument
      assert.equal(complex.arg0.op, Bindings.CHAOS, 'Verify the first ~ in ' + rune);
      assert.equal(complex.arg0.arg0.arg0, Glyphs.fromCharacter('A'), 'Verify A in ' + rune);

      // Check the second argument
      assert.equal(complex.arg1.op, Bindings.LIFE, 'Verify the # in ' + rune);
      assert.equal(complex.arg1.arg0.arg0, Glyphs.fromCharacter('B'), 'Verify the B in ' + rune);
      assert.equal(complex.arg1.arg1.op, Bindings.WATER, 'Verify the & in ' + rune);
      assert.equal(complex.arg1.arg1.arg0.arg0, Glyphs.fromCharacter('C'), 'Verify the C in ' + rune);
      assert.equal(complex.arg1.arg1.arg1.op, Bindings.CHAOS, 'Verify the second ~ in ' + rune);
      assert.equal(complex.arg1.arg1.arg1.arg0.arg0, Glyphs.fromCharacter('D'), 'Verify the D in ' + rune);
    });
  });
  
  describe('#copy', function () {
    it('should create an identical copy', function () {
      var rune = Rune.fromString('(A|(C>B))');
      var copy = rune.copy();
      assert.ok(rune.equals(copy), 'The two should equal');
    });
    it('should not change if the original is changed', function () {
      var rune = Rune.fromString('(A|(C>B))');
      var copy = rune.copy();
      assert.ok(rune.equals(copy), 'The two should equal');
      rune.arg0 = new Rune(Glyphs.fromCharacter('D'));
      assert.ok(!rune.equals(copy), 'The two should no longer equal');
    });
  });
  
  describe('#negate', function () {
    it('should have a chaos binding', function () {
      var negation = Rune.fromString('A').negate();
      assert.equal(Bindings.CHAOS, negation.op, 'The binding should be CHAOS');
    });
    it('should have an arg0 equal to the original', function () {
      var rune = Rune.fromString('(~(C)#(A|(C&F)))');
      var negation = rune.negate();
      assert.ok(rune.equals(negation.arg0), 'arg0 should equal the original');
    });
    it('should be a distinct copy', function () {
      var rune = Rune.fromString('((B|~(D))#(A|(C&F)))');
      var negation = rune.negate();
      assert.ok(rune.equals(negation.arg0), 'arg0 should equal the original');
      rune.arg0 = new Rune(Glyphs.fromCharacter('D'));
      assert.ok(!rune.equals(negation.arg0), 'The two should no longer equal');
    });
  });
  
  describe('#inverse', function () {
    it('should have a CHAOS binding when absent', function () {
      var inverse = Rune.fromString('A').inverse();
      var expected = Rune.fromString('~(A)');
      assert.ok(expected.equals(inverse), 'The binding should be CHAOS');
    });
    it('should remove a CHAOS binding when present', function () {
      var inverse = Rune.fromString('~(A)').inverse();
      var expected = Rune.fromString('A');
      assert.ok(expected.equals(inverse), 'The CHAOS binding should be removed');
    });
    it('should be a distinct copy', function () {
      var rune = Rune.fromString('((B|~(D))#(A|(C&F)))');
      var inverse = rune.inverse();
      assert.ok(rune.equals(inverse.arg0), 'arg0 should equal the original');
      rune.arg0 = new Rune(Glyphs.fromCharacter('D'));
      assert.ok(!rune.equals(inverse.arg0), 'The two should no longer equal');
    });
  });
  
  describe('#fromComponents', function () {
    it('should have the specified binding', function () {
      var arg0 = Rune.fromString('A');
      var arg1 = Rune.fromString('C');
      var rune = Rune.fromComponents(arg0, arg1, Bindings.EARTH);
      assert.equal(Bindings.EARTH, rune.op, 'The binding should be Earth');
    });
    it('should have a copy of arg0', function () {
      var arg0 = Rune.fromString('A');
      var arg1 = Rune.fromString('C');
      var rune = Rune.fromComponents(arg0, arg1, Bindings.EARTH);
      assert.ok(arg0.equals(rune.arg0), 'arg0 should equal the original');
      arg0 = new Rune(Glyphs.fromCharacter('D'));
      assert.ok(!arg0.equals(rune.arg0), 'The two should no longer equal');
    });
    it('should have a copy of arg1', function () {
      var arg0 = Rune.fromString('A');
      var arg1 = Rune.fromString('C');
      var rune = Rune.fromComponents(arg0, arg1, Bindings.EARTH);
      assert.ok(arg1.equals(rune.arg1), 'arg1 should equal the original');
      arg1 = new Rune(Glyphs.fromCharacter('F'));
      assert.ok(!arg1.equals(rune.arg1), 'The two should no longer equal');
    });
  });
});