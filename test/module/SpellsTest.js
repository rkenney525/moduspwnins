/* global TIMEOUT, assert */

describe('Spells', function () {
  // Get Dependencies
  var Spells, Rune, _;
  before(function (done) {
    this.timeout(TIMEOUT);
    require('../setup');
    requirejs(['Spells', 'Rune', 'underscore'], function (_Spells, _Rune, underscore) {
      Spells = _Spells;
      Rune = _Rune;
      _ = underscore;
      done();
    });
  });

  function toStringArray(runeArray) {
    var result = [];
    _.each(runeArray, function (item) {
      result.push(item.toString());
    });
    return result;
  }

  function testBinarySpell(spell, arg0Str, arg1Str, expectedLi) {
    // Setup
    var arg0 = Rune.fromString(arg0Str);
    var arg1 = (arg1Str) ? Rune.fromString(arg1Str) : undefined;
    var result = toStringArray(spell.apply(arg0, arg1));
    
    // Validation
    var inputString = spell.name + " with " + arg0Str + (arg1Str ? ("and " + arg1Str) : "");
    if (_.isEmpty(expectedLi)) {
      assert.ok(_.isEmpty(result), 'The result of ' + inputString + ' should be empty');
    } else {
      _.each(expectedLi, function (expectedItem) {
        assert.ok(_.contains(result, expectedItem), 'The result of ' + inputString + ' should contain ' + expectedItem);
      });
    }
  }
  
  function testBinarySpellForEmpty(spell, arg0Str, arg1Str) {
    testBinarySpell(spell, arg0Str, arg1Str, []);
  }
  
  function testUnarySpell(spell, arg0Str, expectedLi) {
    testBinarySpell(spell, arg0Str, undefined, expectedLi);
  }
  
  function testUnarySpellForEmpty(spell, arg0Str) {
    testUnarySpell(spell, arg0Str, []);
  }

  describe('#AE', function () {
    it('should return empty when arg0 doesnt have an AIR binding', function () {
      testBinarySpellForEmpty(Spells.AE, '(A#C)', 'A');
    });
    it('should return empty when arg0.arg0 is not arg1 ', function () {
      testBinarySpellForEmpty(Spells.AE, '(A>C)', 'B');
    });
    it('should return q given p>q and p', function () {
      testBinarySpell(Spells.AE, '(D>E)', 'D', ['E']);
    });
  });

  describe('#AC', function () {
    it('should return empty when arg0 doesnt have an AIR binding', function () {
      testBinarySpellForEmpty(Spells.AC, '(A#C)', '~(C)');
    });
    it('should return empty when arg1 doesnt have a CHAOS binding', function () {
      testBinarySpellForEmpty(Spells.AC, '(A>C)', 'C');
    });
    it('should return empty when arg0.arg1 is not a CHAOS bound arg1 ', function () {
      testBinarySpellForEmpty(Spells.AC, '(A>C)', '~(B)');
    });
    it('should return ~p given p>q and ~q', function () {
      testBinarySpell(Spells.AC, '(D>E)', '~(E)', ['~(D)']);
    });
  });

  describe('#FS', function () {
    it('should return empty when arg0 doesnt have an FIRE binding', function () {
      testBinarySpellForEmpty(Spells.FS, '(A#B)', '~(A)');
    });
    it('should return empty when arg1 doesnt have a CHAOS binding', function () {
      testBinarySpellForEmpty(Spells.FS, '(A|B)', 'A');
    });
    it('should return empty when arg0.arg0 negated is not arg1 ', function () {
      testBinarySpellForEmpty(Spells.FS, '(A|B)', '~(F)');
    });
    it('should return q given p|q and ~p', function () {
      testBinarySpell(Spells.FS, '(A|C)', '~(A)', ['C']);
    });
  });

  describe('#TD', function () {
    it('should return empty when arg0 doesnt have a WATER binding', function () {
      testBinarySpellForEmpty(Spells.TD, '((A>C)#(B>D))', '(A|B)');
    });
    it('should return empty when arg0.arg0 doesnt have a AIR binding', function () {
      testBinarySpellForEmpty(Spells.TD, '((A|C)&(B>D))', '(A|B)');
    });
    it('should return empty when arg0.arg1 doesnt have a AIR binding', function () {
      testBinarySpellForEmpty(Spells.TD, '((A>C)&(B&D))', '(A|B)');
    });
    it('should return empty when arg1 doesnt have a FIRE binding', function () {
      testBinarySpellForEmpty(Spells.TD, '((A>C)&(B>D))', '(A>B)');
    });
    it('should return empty when arg1.arg0 is not arg0.arg0.arg0', function () {
      testBinarySpellForEmpty(Spells.TD, '((A>C)&(B>D))', '(G|B)');
    });
    it('should return empty when arg1.arg1 is not arg0.arg1.arg0', function () {
      testBinarySpellForEmpty(Spells.TD, '((A>C)&(B>D))', '(A|I)');
    });
    it('should return q|s given (p>q)&(r>s) and p|r', function () {
      testBinarySpell(Spells.TD, '((A>C)&(B>D))', '(A|B)', ['(C|D)']);
    });
  });

  describe('#AR', function () {
    it('should return empty when arg0 doesnt have an AIR binding', function () {
      testBinarySpellForEmpty(Spells.AR, '(A#B)', '(B>C)');
    });
    it('should return empty when arg1 doesnt have an AIR binding', function () {
      testBinarySpellForEmpty(Spells.AR, '(A>B)', '(B&C)');
    });
    it('should return empty when arg0.arg1 is not a arg1.arg0 ', function () {
      testBinarySpellForEmpty(Spells.AR, '(A>D)', '(B>C)');
    });
    it('should return p>r given p>q and q>r', function () {
      testBinarySpell(Spells.AR, '(A>B)', '(B>C)', ['(A>C)']);
    });
  });

  describe('#ER', function () {
    it('should return empty when arg0 doesnt have a WATER binding', function () {
      testUnarySpellForEmpty(Spells.ER, '((D>C)#B)');
    });
    it('should return p given p&q', function () {
      testUnarySpell(Spells.ER, '((D>C)&B)', ['(D>C)']);
    });
  });

  describe('#AQ', function () {
    it('should return p&q given p and q', function () {
      testBinarySpell(Spells.AQ, '(F>C)', '~((I#A))', ['((F>C)&~((I#A)))']);
    });
  });

  describe('#DI', function () {
    it('should return empty when arg0 doesnt have an AIR binding ', function () {
      testUnarySpellForEmpty(Spells.DI, '(A|B)');
    });
    it('should return both p>q and p>(p&(p&q)) given (p>(p&q))', function () {
      testUnarySpell(Spells.DI, '(A>(A&B))', ['(A>B)', '(A>(A&(A&B)))']);
    });
    it('should return (p>(p&q)) given (p>q)', function () {
      testUnarySpell(Spells.DI, '(A>B)', ['(A>(A&B))']);
    });
  });

  describe('#BF', function () {
    it('should return p|q given p and q', function () {
      testBinarySpell(Spells.BF, '(F>C)', '~((I#A))', ['((F>C)|~((I#A)))']);
    });
  });

  describe('#CI', function () {
    it('should return empty when arg0 doesnt have a WATER, FIRE, or CHAOS binding ', function () {
      testUnarySpellForEmpty(Spells.CI, '(A>B)');
    });
    it('should return empty when arg0 is CHAOS bound but doesnt have a WATER or FIRE binding internally', function () {
      testUnarySpellForEmpty(Spells.CI, '~((A>B))');
    });
    it('should return ~(p|q) given ~p&~q', function () {
      testUnarySpell(Spells.CI, '(~(A)&~(B))', ['~((A|B))']);
    });
    it('should return ~(~p|q) given p&~q', function () {
      testUnarySpell(Spells.CI, '(A&~(B))', ['~((~(A)|B))']);
    });
    it('should return p&~q given ~(p|q)', function () {
      testUnarySpell(Spells.CI, '~((A|B))', ['(~(A)&~(B))']);
    });
    it('should return p&~q given ~(~p|q)', function () {
      testUnarySpell(Spells.CI, '~((~(A)|B))', ['(A&~(B))']);
    });
  });

  describe('#MI', function () {
    it('should return empty when arg0 doesnt have a WATER or FIRE binding ', function () {
      testUnarySpellForEmpty(Spells.MI, '(A>B)');
    });
    it('should return (q|p) given (p|q)', function () {
      testUnarySpell(Spells.MI, '(A|(C&~(D)))', ['((C&~(D))|A)']);
    });
    it('should return (q&p) given (p&q)', function () {
      testUnarySpell(Spells.MI, '(A&(C>~(D)))', ['((C>~(D))&A)']);
    });
  });

  describe('#TR', function () {
    it('should return empty when arg0 doesnt have a WATER or FIRE binding ', function () {
      testUnarySpellForEmpty(Spells.TR, '(A>B)');
    });
    it('should return empty given unmatching Bindings', function () {
      testUnarySpellForEmpty(Spells.TR, '(A|(B&C))');
    });
    it('should return ((p|q)|r) given (p|(q|r))', function () {
      testUnarySpell(Spells.TR, '(A|(B|C))', ['((A|B)|C)']);
    });
    it('should return (p|(q|r)) given ((p|q)|r)', function () {
      testUnarySpell(Spells.TR, '((A|B)|C)', ['(A|(B|C))']);
    });
    it('should return both (p|(q|(r|s))) and (((p|q)|r)|s) given ((p|q)|(r|s))', function () {
      testUnarySpell(Spells.TR, '((A|B)|(C|D))', ['(A|(B|(C|D)))', '(((A|B)|C)|D)']);
    });
    it('should return ((p&q)&r) given (p&(q&r))', function () {
      testUnarySpell(Spells.TR, '(A&(B&C))', ['((A&B)&C)']);
    });
    it('should return (p&(q&r)) given ((p&q)&r)', function () {
      testUnarySpell(Spells.TR, '((A&B)&C)', ['(A&(B&C))']);
    });
    it('should return both (p&(q&(r&s))) and (((p&q)&r)&s) given ((p&q)&(r&s))', function () {
      testUnarySpell(Spells.TR, '((A&B)&(C&D))', ['(A&(B&(C&D)))', '(((A&B)&C)&D)']);
    });
  });

  describe('#ST', function () {
    it('should return empty when arg0 doesnt have a WATER or FIRE binding ', function () {
      testUnarySpellForEmpty(Spells.ST, '(A>(B|C))');
    });
    it('should return empty when arg0.arg1 doesnt have a WATER or FIRE binding ', function () {
      testUnarySpellForEmpty(Spells.ST, '(A|(B>C))');
    });
    it('should return empty when arg0.arg0 doesnt have a WATER or FIRE binding ', function () {
      testUnarySpellForEmpty(Spells.ST, '((B>D)|(B>C))');
    });
    it('should return empty given matching Bindings', function () {
      testUnarySpellForEmpty(Spells.ST, '(A|(B|C))');
    });
    it('should return ((p|r)&(p|r)) given (p|(q&r))', function () {
      testUnarySpell(Spells.ST, '(A|(B&C))', ['((A|B)&(A|C))']);
    });
    it('should return ((p&r)|(p&r)) given (p&(q|r))', function () {
      testUnarySpell(Spells.ST, '(A&(B|C))', ['((A&B)|(A&C))']);
    });
    it('should return (p|(q&r) given ((p|r)&(p|r)))', function () {
      testUnarySpell(Spells.ST, '((A|B)&(A|C))', ['(A|(B&C))']);
    });
    it('should return (p&(q|r) given ((p&r)|(p&r)))', function () {
      testUnarySpell(Spells.ST, '((A&B)|(A&C))', ['(A&(B|C))']);
    });
    // TODO find a multi-result example
  });

  describe('#OC', function () {
    it('should return ~~p given p', function () {
      testUnarySpell(Spells.OC, 'C', ['~(~(C))']);
    });
    it('should return both ~~~~p and p given ~~p', function () {
      testUnarySpell(Spells.OC, '~(~((A>B)))', ['~(~(~(~((A>B)))))', '(A>B)']);
    });
  });

  describe('#DW', function () {
    it('should return empty when arg0 doesnt have an AIR binding ', function () {
      testUnarySpellForEmpty(Spells.DW, '(A|B)');
    });
    it('should return ~q>~p given p>q', function () {
      testUnarySpell(Spells.DW, '(A>B)', ['(~(B)>~(A))']);
    });
    it('should return q>p given ~p>~q', function () {
      testUnarySpell(Spells.DW, '(~(A)>~(B))', ['(B>A)']);
    });
    it('should return ~q>p given ~p>q', function () {
      testUnarySpell(Spells.DW, '(~(A)>B)', ['(~(B)>A)']);
    });
    it('should return q>~p given p>~q', function () {
      testUnarySpell(Spells.DW, '(A>~(B))', ['(B>~(A))']);
    });
  });

  describe('#CO', function () {
    it('should return empty when arg0 doesnt have an AIR or FIRE binding ', function () {
      testUnarySpellForEmpty(Spells.CO, '(A&B)');
    });
    it('should return ~p|q given p>q', function () {
      testUnarySpell(Spells.CO, '(A>B)', ['(~(A)|B)']);
    });
    it('should return p|q given ~p>q', function () {
      testUnarySpell(Spells.CO, '(~(A)>B)', ['(A|B)']);
    });
    it('should return p>q given ~p|q ', function () {
      testUnarySpell(Spells.CO, '(~(A)|B)', ['(A>B)']);
    });
    it('should return ~p>q given p|q', function () {
      testUnarySpell(Spells.CO, '(A|B)', ['(~(A)>B)']);
    });
  });

  describe('#TE', function () {
    it('should return empty when arg0 doesnt have an EARTH, WATER, or FIRE binding ', function () {
      testUnarySpellForEmpty(Spells.TE, '(A&B)');
    });
    it('should return both ((p>q)&(q>p)) and ((p&q)|(~p&~q)) given p%q', function () {
      testUnarySpell(Spells.TE, '(A%B)', ['((A>B)&(B>A))', '((A&B)|(~(A)&~(B)))']);
    });
    it('should return p%q given ((p>q)&(q>p))', function () {
      testUnarySpell(Spells.TE, '((A>B)&(B>A))', ['(A%B)']);
    });
    it('should return p%q given ((p&q)|(~p&~q))', function () {
      testUnarySpell(Spells.TE, '((A&B)|(~(A)&~(B)))', ['(A%B)']);
    });
  });

  describe('#TC', function () {
    it('should return empty when arg0 doesnt have an AIR binding ', function () {
      testUnarySpellForEmpty(Spells.TC, '(A&(B>C))');
    });
    it('should return p>(q>r) given (p&q)>r', function () {
      testUnarySpell(Spells.TC, '((A&B)>C)', ['(A>(B>C))']);
    });
    it('should return (p&q)>r given p>(q>r)', function () {
      testUnarySpell(Spells.TC, '(A>(B>C))', ['((A&B)>C)']);
    });
    it('should return both p>(q>(r>s)) and ((p&q)&r)>s given (p&q)>(r>s)', function () {
      testUnarySpell(Spells.TC, '((A&B)>(C>D))', ['(A>(B>(C>D)))', '(((A&B)&C)>D)']);
    });
  });

  describe('#FR', function () {
    it('should return (p|p) given p', function () {
      testUnarySpell(Spells.FR, 'A', ['(A|A)']);
    });
    it('should return both p and ((p|p)|(p|p)) given p|p', function () {
      testUnarySpell(Spells.FR, '(A|A)', ['A', '((A|A)|(A|A))']);
    });
  });

  describe('#GL', function () {
    it('should return null when arg0 doesnt have a LIFE binding', function () {
      testBinarySpellForEmpty(Spells.GL, '(A|B)', '~(A)');
    });
    it('should return q given p#q and ~p', function () {
      testBinarySpell(Spells.GL, '(A#B)', '~(A)', ['B']);
    });
    it('should return ~q given p#q and p', function () {
      testBinarySpell(Spells.GL, '(A#B)', 'A', ['~(B)']);
    });
  });

  describe('#isUnary', function () {
    it('should follow the described behavior for all Spells', function () {
      assert.ok(!Spells.isUnary(Spells.AE), 'AE is not unary');
      assert.ok(!Spells.isUnary(Spells.AC), 'AC is not unary');
      assert.ok(!Spells.isUnary(Spells.FS), 'FS is not unary');
      assert.ok(!Spells.isUnary(Spells.TD), 'TD is not unary');
      assert.ok(!Spells.isUnary(Spells.AR), 'AR is not unary');
      assert.ok(Spells.isUnary(Spells.ER), 'AR is unary');
      assert.ok(!Spells.isUnary(Spells.AQ), 'AQ is not unary');
      assert.ok(Spells.isUnary(Spells.DI), 'DI is unary');
      assert.ok(!Spells.isUnary(Spells.BF), 'BF is not unary');
      assert.ok(Spells.isUnary(Spells.CI), 'CI is unary');
      assert.ok(Spells.isUnary(Spells.MI), 'MI is unary');
      assert.ok(Spells.isUnary(Spells.TR), 'TR is unary');
      assert.ok(Spells.isUnary(Spells.ST), 'ST is unary');
      assert.ok(Spells.isUnary(Spells.OC), 'OC is unary');
      assert.ok(Spells.isUnary(Spells.DW), 'DW is unary');
      assert.ok(Spells.isUnary(Spells.CO), 'CO is unary');
      assert.ok(Spells.isUnary(Spells.TE), 'TE is unary');
      assert.ok(Spells.isUnary(Spells.TC), 'TC is unary');
      assert.ok(Spells.isUnary(Spells.FR), 'FR is unary');
      assert.ok(!Spells.isUnary(Spells.GL), 'GL is not unary');
    });
  });
});