/* global TIMEOUT, assert */

describe('Glyphs', function () {
  // Get Dependencies
  var Glyphs, _;
  before(function (done) {
    this.timeout(TIMEOUT);
    require("../setup");
    requirejs(['Glyphs', 'underscore'], function (_Glyphs, underscore) {
      Glyphs = _Glyphs;
      _ = underscore;
      done();
    });
  });

  describe('#values', function () {
    it('should contain all glyphs', function () {
      var values = Glyphs.values();
      assert.ok(_.contains(values, Glyphs.AT), 'Values should contain AT');
      assert.ok(_.contains(values, Glyphs.NA), 'Values should contain NA');
      assert.ok(_.contains(values, Glyphs.REE), 'Values should contain REE');
      assert.ok(_.contains(values, Glyphs.TA), 'Values should contain TA');
      assert.ok(_.contains(values, Glyphs.LO), 'Values should contain LO');
      assert.ok(_.contains(values, Glyphs.SHA), 'Values should contain SHA');
      assert.ok(_.contains(values, Glyphs.DOE), 'Values should contain DOE');
      assert.ok(_.contains(values, Glyphs.FOO), 'Values should contain FOO');
      assert.ok(_.contains(values, Glyphs.GU), 'Values should contain GU');
    });
    it('should return a fresh copy each time', function() {
      // Get the normal state
      var values = Glyphs.values();
      var unmodifiedLength = values.length;
      assert.equal(values.length, unmodifiedLength, 'Make sure we stored the length correctly');
      
      // Modify the array and assert it has been changed
      values.pop();
      values.pop();
      values.pop();
      assert.equal(values.length, unmodifiedLength - 3, 'Make sure we have 3 fewer than original');
      
      // Try it on a brand new call
      assert.equal(Glyphs.values().length, unmodifiedLength, 'New call should be at original length');
    });
  });
  
  describe('#fromCharacter', function () {
    it('should return undefined for an invalid character', function() {
      assert.equal(undefined, Glyphs.fromCharacter('?'), '? is not a character for any Glyph');
    });
    
    it('should return the Glyph for the corresponding character', function() {
      assert.equal(Glyphs.AT, Glyphs.fromCharacter('A'), '"A" should yield AT');
      assert.equal(Glyphs.NA, Glyphs.fromCharacter('B'), '"B" should yield AT');
      assert.equal(Glyphs.REE, Glyphs.fromCharacter('C'), '"C" should yield AT');
      assert.equal(Glyphs.TA, Glyphs.fromCharacter('D'), '"D" should yield AT');
      assert.equal(Glyphs.LO, Glyphs.fromCharacter('E'), '"E" should yield AT');
      assert.equal(Glyphs.SHA, Glyphs.fromCharacter('F'), '"F" should yield AT');
      assert.equal(Glyphs.DOE, Glyphs.fromCharacter('G'), '"G" should yield AT');
      assert.equal(Glyphs.FOO, Glyphs.fromCharacter('H'), '"H" should yield AT');
      assert.equal(Glyphs.GU, Glyphs.fromCharacter('I'), '"I" should yield AT');
    });
  });
});