/* global TIMEOUT, sinon, assert */

require("../setup");

describe('MenuScene', function () {
  // Get Dependencies
  var MenuScene, $;
  before(function (done) {
    this.timeout(TIMEOUT);
    // Load the MenuScene
    requirejs(['jquery', 'MenuScene'], function (_$, _MenuScene) {
      $ = _$;
      MenuScene = _MenuScene;
      done();
    });
  });

  // Create a new instance each time
  var instance;
  var startSceneStub = sinon.stub();
  beforeEach(function () {
    instance = new MenuScene({
      $stage: $('#stage'),
      startScene: startSceneStub
    });
  });

  describe('#exit', function () {
    it('should close the window', function () {
      var exitSpy = sinon.spy(window, 'close');

      instance.exit();

      assert.ok(exitSpy.calledOnce, 'window.exit should be called');
    });
  });
});