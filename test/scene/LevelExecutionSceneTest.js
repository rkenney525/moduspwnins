/* global TIMEOUT, sinon, assert */

require("../setup");

describe('LevelExecutionScene', function () {
  // Get Dependencies
  var LevelExecutionScene, Rune, Spells, $, _;
  before(function (done) {
    this.timeout(TIMEOUT);
    // Load the MenuScene
    requirejs(['jquery', 'underscore', 'LevelExecutionScene', 'Rune', 'Spells'],
        function (_$, underscore, _LevelExecutionScene, _Rune, _Spells) {
          $ = _$;
          _ = underscore;
          LevelExecutionScene = _LevelExecutionScene;
          Rune = _Rune;
          Spells = _Spells;
          done();
        });
  });

  // Create a new instance each time
  var instance, instance;
  var startSceneStub = sinon.stub();
  beforeEach(function () {
    instance = new LevelExecutionScene({
      $stage: $('#stage'),
      startScene: startSceneStub
    });
    instance.start({
      runes: [Rune.fromString('A'), Rune.fromString('B'), Rune.fromString('C'),
        Rune.fromString('D'), Rune.fromString('E'), Rune.fromString('F'),
        Rune.fromString('G'), Rune.fromString('H'), Rune.fromString('I')],
      spells: [Spells.AE, Spells.AQ, Spells.DI]
    });
  });

  describe('#start', function () {
    it('should set and display the runes', function () {
      // #start was called in beforeEach
      assert.equal(instance.runes.length, 9, 'There should be nine Runes');
      assert.equal($('#RuneSlabContainer').children().size(), 7, 'There should be seven Runes on display');
    });
    it('should set and display the spells', function () {
      // #start was called in beforeEach
      assert.equal(instance.spells.length, 3, 'There should be three Spells');
      var allPopulated = _.every($('.spell-description-contents,.spell-control-contents'), function (div) {
        return !_.isEmpty($(div).html());
      });
      assert.ok(allPopulated, 'Both containers should have content');
    });
  });

  describe('#runePagerNext', function () {
    it('should move the Rune Pager to the next page and display it', function () {
      instance.runePagerNext();
      assert.equal(instance.runePager.currentPage, 2, 'Should be on the second page');
      assert.equal($('#RuneSlabContainer').children().first().data('recipe'), 'H', 'The first element on the second page is H');
    });
  });

  describe('#runePagerPrev', function () {
    it('should move the Rune Pager to the previous page and display it', function () {
      instance.runePagerNext();
      instance.runePagerPrev();
      assert.equal(instance.runePager.currentPage, 1, 'Should be on the first page');
      assert.equal($('#RuneSlabContainer').children().first().data('recipe'), 'A', 'The first element on the first page is A');
    });
  });

  describe('#spellPagerNext', function () {
    it('should move the Spell Pager to the next page and display it', function () {
      instance.spellPagerNext();
      assert.equal(instance.spellPager.currentPage, 2, 'Should be on the second page');
      assert.equal($('#SpellTitle').text(), Spells.AQ.name, 'The second Spell is Spells.AQ');
    });
  });

  describe('#spellPagerPrev', function () {
    it('should move the Spell Pager to the previous page and display it', function () {
      instance.spellPagerNext();
      instance.spellPagerPrev();
      assert.equal(instance.spellPager.currentPage, 1, 'Should be on the first page');
      assert.equal($('#SpellTitle').text(), Spells.AE.name, 'The second Spell is Spells.AE');
    });
  });

  describe('#displayRunes', function () {
    it('should display the provided runes', function () {
      instance.displayRunes([Rune.fromString('A'), Rune.fromString('B'), Rune.fromString('C')]);
      assert.equal($('#RuneSlabContainer').children().size(), 3, 'The Rune Container should have three Runes in it');
    });
    it('should add the selectable-rune class to the runes', function () {
      instance.displayRunes([Rune.fromString('A')]);
      assert.ok($('#RuneSlabContainer').children().hasClass('selectable-rune'), 'The Rune should have the selectable-rune class');
    });
  });

  describe('#displaySpell', function () {
    it('should display the name as the title', function () {
      instance.displaySpell([Spells.AR]);
      assert.equal($('#SpellTitle').text(), Spells.AR.name, 'The title should be displayed');
    });
    it('should display a row for each form', function () {
      instance.displaySpell([Spells.AR]);
      assert.equal($('#SpellFormsTable tbody tr').size(), Spells.AR.forms.length, 'Every form should have a row');
    });
    it('should display one argument box for a unary spell', function () {
      instance.displaySpell([Spells.ER]);
      assert.equal($('.spell-argument').size(), 1, 'There should only be one argument box');
    });
    it('should display two argument boxs for a binary spell', function () {
      instance.displaySpell([Spells.AQ]);
      assert.equal($('.spell-argument').size(), 2, 'There should only be one argument box');
    });
  });

  describe('#selectRune', function () {
    it('should select the specified rune', function () {
      var rune = $('.selectable-rune')[3];
      instance.selectRune({
        currentTarget: rune
      });
      assert.ok($(rune).hasClass(instance.SELECTED_CLASS), 'Rune should have gained the selected class');
    });
    it('should deselect a previously specified rune', function () {
      var otherRune = $('.selectable-rune')[3];
      var newRune = $('.selectable-rune')[1];
      instance.selectRune({
        currentTarget: otherRune
      });
      instance.selectRune({
        currentTarget: newRune
      });
      assert.ok(!$(otherRune).hasClass(instance.SELECTED_CLASS), 'new Rune should not have the selected class');
    });
    it('should add helpers to both (assuming both empty) argument containers', function () {
      instance.selectRune({
        currentTarget: $('.selectable-rune')[3]
      });
      assert.equal($('.arg-helper').size(), 2, 'There should be two helpers');
    });
  });

  describe('#engageArgument', function () {
    it('should add the selected rune', function () {
      var $arg = $('.spell-argument');
      var $rune = $('.selectable-rune');
      instance.selectRune({
        currentTarget: $rune[3]
      });
      instance.engageArgument({
        currentTarget: $arg[0]
      });
      assert.ok($arg.first().children().data('recipe'),
          $($rune[3]).children().data('recipe'),
          'There should be some content');
    });
    it('should remove an existing rune if there is no selected rune', function () {
      var $arg = $('.spell-argument');
      var $rune = $('.selectable-rune');
      instance.selectRune({
        currentTarget: $rune[3]
      });
      instance.engageArgument({
        currentTarget: $arg[0]
      });
      // Set this to make the rune disappear immediately
      instance.FADE_TIME = -1;
      instance.engageArgument({
        currentTarget: $arg[0]
      });
      assert.ok(_.isEmpty($arg.html()), 'The arg should be empty after being selected again');
    });
  });
  
  describe('#engageResult', function () {
    it('should add a result to the list of runes', function () {
      $('#Result').html(Rune.fromString('(A>B)').html());
      var oldLength = instance.runes.length;
      instance.engageResult();
      assert.equal(instance.runes.length, oldLength + 1, 'There should be an additional Rune');
    });
    it('immediately show the new Rune if on the last page', function () {
      $('#Result').html(Rune.fromString('(A>B)').html());
      instance.runePagerNext();
      var displayed = $('#RuneSlabContainer').children().size();
      instance.engageResult();
      assert.equal($('#RuneSlabContainer').children().size(), displayed + 1, 'There should be an additional Rune displayed');
    });
  });
  
  describe('#addHelpers', function () {
    it('should add two helpers if there are two slots open', function () {
      instance.addHelpers();
      assert.equal($('.arg-helper').size(), 2, 'There should be two helpers');
    });
    it('should add one helper if there is one slot open', function () {
      $('.spell-argument').first().html(Rune.fromString('(C|H)').html());
      instance.addHelpers();
      assert.equal($('.arg-helper').size(), 1, 'There should be one helper');
    });
    it('should add no helpers if there are no slots open', function () {
      $('.spell-argument').html(Rune.fromString('(C|H)').html());
      instance.addHelpers();
      assert.equal($('.arg-helper').size(), 0, 'There should be no helpers');
    });
  });
  
  describe('#removeHelpers', function () {
    it('should remove all helpers', function () {
      instance.addHelpers();
      instance.removeHelpers();
      assert.equal($('.arg-helper').size(), 0, 'There should be no helpers');
    });
  });
  
  describe('#applySpell', function () {
    it('should add the result of a Spell to the result', function () {
      $('.spell-argument').first().html(Rune.fromString('(A>B)').html());
      $('.spell-argument').last().html(Rune.fromString('A').html());
      instance.applySpell();
      assert.equal($('#Result').children().data('recipe'), 'B', 'The result of AE on (A>B) and A is B');
    });
  });
});