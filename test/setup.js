// Set basic testing tools
sinon = require("sinon");
assert = require("assert");

// Configure settings
TIMEOUT = 2000;

// Get requirejs
requirejs = require("requirejs");
requirejs.config({
  nodeRequire: require
});
require("../dependencies");

// Create a DOM
jsdom = require('jsdom');
var fs = require('fs');
jsdom.env({
  html: fs.readFileSync('build/stage/moduspwnins.html', 'utf-8'),
  done: function (err, _window) {
    window = _window;
  }
});
