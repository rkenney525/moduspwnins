# Modus Pwnins
Modus Pwnins is a logic based puzzle game that challenges your pattern matching abilities and memory.

## Prerequisites
 - Node.js at *4.x*
 - grunt-cli at *latest*

## Instructions for Running
 - Run `npm install` in the root directory
 - Run `grunt` in the root directory (be sure you have grunt-cli installed)
 - Navigate to `build/ModusPwnins`
 - Pick a version (only win32 and win64 at the moment) by selecting the appropriate directory
 - Execute `ModusPwnins.exe`

## Save Data Format
This section overviews all save data keys and the type of value stored. All Values are Objects with one or more 
properties. Each property will list the data type and a short description

### Players Level Stats
Statistics concerning the player's performance on a particular level, identified in the key.

#### Key
Level identifier - A Level number preceded by `LEVEL_`

#### Values
##### best: Number
The best score on a particular Level (ie the lowest number)

### Current Level Information
Information pertaining to the most recent Level the player has loaded

#### Key: `currentLevel`
#### Values
##### index: Number
The index this Level has in the global list of Levels

##### progress: String
A parsable Level Object stored as a String containing all Facts that have been established so far

### User created Facts
Contains the Facts a user has created

#### Key: `addTable`
#### Values
##### AT1: String
The first Fact in the AddTable
##### AT2: String
The second Fact in the AddTable
##### AT3: String
The third Fact in the AddTable
##### AT4: String
The fourth Fact in the AddTable
##### AT5: String
The fifth Fact in the AddTable