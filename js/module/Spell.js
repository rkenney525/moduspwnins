define(['jquery', 'underscore', 'Rune'], function ($, _, Rune) {
  /**
   * Create a spell of a given type with the speciefied name. Functionality is provided in the apply function.
   * @param {String} name The name of the Spell
   * @param {Spell.Type} type The SpellType for this Spell
   * @param {Array[Spell.Form]} forms An array of objects mapping each possible result to an array of inputs
   * @param {Function} apply The application of this spell. Function should accept one or two Runes.
   * @returns {Spell} The created Spell
   */
  var Spell = function (name, type, forms, apply) {
    this.name = name;
    this.type = type;
    this.forms = forms;
    this.apply = function(arg0, arg1) {
      try {
        return apply(arg0, arg1);
      } catch (anything) {
        return [];
      }
    };
  };

  /**
   * Maps a letter (Glyph) to an arbitrary shape. Internal use only.
   */
  Spell.letterMappings = {
    'A': '&#9632;',
    'B': '&#9650;',
    'C': '&#9670;',
    'D': '&#9679;'
  };

  /**
   * Represents a valid form of the Spell. Yields result when apply is called with the inputs
   * 
   * @param {String} result The result of an apply with the provided inputs
   * @param {Array[String]} inputs An array of inputs that yield the result
   * @returns {Spell.Form} A Form of the Spell
   */
  Spell.Form = function (result, inputs) {
    function convertToHtml(str) {
      var $rune = $(Rune.fromString(str).innerHtml().trim());
      if ($rune.is('svg')) {
        // put the svg in a div so that it has a container
        $rune = $('<div></div>').append($rune);
      }
      _.each($rune.find('svg'), function (glyph) {
        var $glyph = $(glyph);
        $glyph.parent().html(Spell.letterMappings[$glyph.text().trim()]);
      });
      return $rune[0].outerHTML;
    }

    this.result = convertToHtml(result);
    this.inputs = [];
    var ref = this;
    _.each(inputs, function (input) {
      ref.inputs.push(convertToHtml(input));
    });
  };

  // String values are the original logical name
  Spell.Type = Object.freeze({MANIPULATION: "Replacement", GENERATION: "Inference"});

  return Spell;
});