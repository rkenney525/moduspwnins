define(['Glyph'], function (Glyph) {
  // Create the initial Glyphs object which houses all instances of a Glyph
  var Glyphs = {
    AT: new Glyph('A'),
    NA: new Glyph('B'),
    REE: new Glyph('C'),
    TA: new Glyph('D'),
    LO: new Glyph('E'),
    SHA: new Glyph('F'),
    DOE: new Glyph('G'),
    FOO: new Glyph('H'),
    GU: new Glyph('I')
  };
  
  /**
   * Return all Glyph values
   * @returns {Array[Glyph]}
   */
  Glyphs.values = function () {
    return [Glyphs.AT, Glyphs.NA, Glyphs.REE, Glyphs.TA, Glyphs.LO, Glyphs.SHA, Glyphs.DOE, Glyphs.FOO, Glyphs.GU];
  };
  
  /**
   * Get a Glyph represented by character, if it exists.
   * @param {String} character The character representation of the desired Glyph
   * @returns {Glyph|undefined} The resulting Glyph, or undefined if there isn't one
   */
  Glyphs.fromCharacter = function(character) {
    var glyph;
    _.some(Glyphs.values(), function(value) {
      if (value.character === character) {
        glyph = value;
        return true;
      }
    });
    return glyph;
  };

  return Glyphs;
});