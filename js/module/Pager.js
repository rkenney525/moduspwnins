define([], function () {
  /**
   * Create a Pager, which manages which objects in list should be displayed 
   * based on the current page.
   * @param {Array} list The list of objects to be paged
   * @param {Number} perPage The number of objects to return per page
   * @param {Function} displayLogic Functionality for displaying the current contents, which are passed as a parameter
   * @param {JQuery} $prevControl JQuery object for the prev control for this pager
   * @param {JQuery} $nextControl JQuery object for the next control for this pager
   * @returns {Pager} The Pager instance
   */
  var Pager = function (list, perPage, displayLogic, $prevControl, $nextControl) {
    this.list = list;
    this.perPage = perPage;
    this.currentPage = 1;
    this.displayLogic = displayLogic;
    this.$prevControl = $prevControl;
    this.$nextControl = $nextControl;
  };

  /**
   * Get the contents of the current page.
   * @returns {Array} The contents of the current page
   */
  Pager.prototype.contents = function () {
    var start = (this.currentPage - 1) * this.perPage;
    var end = start + this.perPage;
    return this.list.slice(start, end);
  };

  /**
   * Move to the next page, if possible
   * @returns {Array} The copntents of the current page
   */
  Pager.prototype.next = function () {
    if (!this.isLast()) {
      this.currentPage++;
    }
    return this.contents();
  };
  
  /**
   * Calls this.next and then displays that information
   * @returns {undefined}
   */
  Pager.prototype.nextAndDisplay = function () {
    var results = this.next();
    this.display(results);
  };

  /**
   * Move to the previous page, if possible
   * @returns {Array} The copntents of the current page
   */
  Pager.prototype.prev = function () {
    if (!this.isFirst()) {
      this.currentPage--;
    }
    return this.contents();
  };
  
  /**
   * Calls this.prev and then displays that information
   * @returns {undefined}
   */
  Pager.prototype.prevAndDisplay = function () {
    var results = this.prev();
    this.display(results);
  };
  
  /**
   * Use the displayLogic to display the current contents
   * @returns {undefined}
   */
  Pager.prototype.display = function () {
    this.updateControls();
    this.displayLogic(this.contents());
  };

  /**
   * Check if this Pager is on the first page.
   * @returns {Boolean} True if this Pager is on the first page, and false otherwise
   */
  Pager.prototype.isFirst = function () {
    return this.currentPage === 1;
  };

  /**
   * Check if this Pager is on the last page.
   * @returns {Boolean} True if this Pager is on the first page, and false otherwise
   */
  Pager.prototype.isLast = function () {
    return this.currentPage === Math.ceil(this.list.length / this.perPage);
  };

  /**
   * Update prev and next controls with the hidden class if they should be hidden, and remove it if they should be 
   * displayed
   * @returns {undefined}
   */
  Pager.prototype.updateControls = function () {
    if (this.isFirst()) {
      this.$prevControl.addClass('hidden');
    } else {
      this.$prevControl.removeClass('hidden');
    }
    if (this.isLast()) {
      this.$nextControl.addClass('hidden');
    } else {
      this.$nextControl.removeClass('hidden');
    }
  };

  return Pager;
});