define(['jquery', 'underscore'], function ($, _) {
  /**
   * Create a Glyph instance
   * 
   * @param {String} character The character used for display purposes and for
   * internal representation.
   * @returns {Glyph} The resulting Glyph instance
   */
  var Glyph = function(character) {
    this.character = character;
  };
  
  /**
   * The template used to render HTML
   */
  Glyph.template = _.template($('#glyphTemplate').html());
  
  /**
   * Get the HTML form of this Glyph.
   * 
   * @returns {String} The HTML that renders the Glyph
   */
  Glyph.prototype.html = function() {
    return Glyph.template({
      character: this.character
    }).trim();
  };
  
  /**
   * Checks this Glyph and other for equivalence.
   * 
   * @param {Glyph} other Another Glyph
   * @returns {Boolean} True if the Glyphs are identical instances, false otherwise
   */
  Glyph.prototype.equals = function(other) {
    return this === other;
  };

  // Return the Glyph class
  return Glyph;
});