define(['Binding', 'underscore'], function (Binding, _) {
  var Bindings = {
    /**
     * Equivalent to INCLUSIVE OR
     */
    FIRE: new Binding('binding-fire', '|'),
    /**
     * Equivalent to AND
     */
    WATER: new Binding('binding-water', '&'),
    /**
     * Equivalent to CONDITIONAL
     */
    AIR: new Binding('binding-air', '>'),
    /**
     * Equivalent to BICONDITIONAL
     */
    EARTH: new Binding('binding-earth', '%'),
    /**
     * Equivalent to EXCLUSIVE OR
     */
    LIFE: new Binding('binding-life', '#'),
    /**
     * Equivalent to NEGATION
     */
    CHAOS: new Binding('binding-chaos', '~')
  };
  
  /**
   * Return all Binding values
   * @returns {Array[Binding]}
   */
  Bindings.values = function() {
    return [Bindings.FIRE, Bindings.WATER, Bindings.AIR, Bindings.EARTH, Bindings.LIFE, Bindings.CHAOS];
  };
  
  /**
   * Get a Binding based on the provided logical representation character.
   * @param {String} character The character representation of the Binding to retrieve
   * @returns {Binding|undefined} The Binding for the specific character, or undefined if one doesn't exist
   */
  Bindings.fromCharacter = function (character) {
    var binding;
    _.some(Bindings.values(), function(value) {
      if (value.character === character) {
        binding = value;
        return true;
      }
    });
    return binding;
  };

  return Bindings;
});