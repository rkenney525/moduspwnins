define(['Glyphs', 'Bindings', 'underscore', 'jquery'], function (Glyphs, Bindings, _, $) {
  /**
   * Creates a Rune with the specified glyphs and binding
   * @param {Glyph} arg0 The first Glyph
   * @param {Glyph} arg1 The second Glyph
   * @param {Binding} op The Binding
   * @returns {Rune} A Rune with the specified glyphs and binding
   */
  var Rune = function (arg0, arg1, op) {
    this.arg0 = arg0;
    this.arg1 = arg1;
    this.op = op;
  };

  /**
   * The template used to render HTML
   */
  Rune.template = _.template($('#runeTemplate').html());

  /**
   * Create the inner HTML for a Rune. The inner HTML is just the HTML for all bindings and glyphs, and not the containing
   * object itself.
   * @param {Boolean} stacked Whether Glyphs should be stacked. If false, they should be side by side
   * @returns {String} The HTML for the Rune
   */
  Rune.prototype.innerHtml = function (stacked) {
    if (this.op === undefined) {
      return this.arg0.html();
    } else if (this.arg1 === undefined) {
      return this.op.html(this.arg0.innerHtml(stacked));
    } else {
      return this.op.html(this.arg0.innerHtml(!stacked), this.arg1.innerHtml(!stacked), stacked);
    }
  };

  /**
   * Create the HTML for a rune, to include the HTML of all components and the container.
   * @returns {String} The HTML for the Rune
   */
  Rune.prototype.html = function () {
    return Rune.template({
      recipe: this.toString(),
      content: this.innerHtml(false)
    }).trim();
  };

  /**
   * Returns the String form of the Rune that can be parsed by Rune.fromString()
   * @returns {String} The String form of the Fact that can be parsed by Rune.fromString()
   */
  Rune.prototype.toString = function () {
    if (this.op === undefined) {
      return this.arg0.character;
    } else if (this.arg1 === undefined) {
      return this.op.character + "(" + this.arg0.toString() + ")";
    } else {
      return "(" + this.arg0.toString() + this.op.character +
          this.arg1.toString() + ")";
    }
  };

  /**
   * Test other for equality against the invoking Rune.
   * 
   * @param {Rune} other The Rune to check
   * @returns {Boolean} True if the Runes are identical, false otherwise
   */
  Rune.prototype.equals = function (other) {
    return this.toString() === other.toString();
  };

  /**
   * Creates a seperate copy of the invoking Rune
   * @returns {Rune} The copy of the invoking Rune
   */
  Rune.prototype.copy = function () {
    return Rune.fromString(this.toString());
  };

  /**
   * Returns the negated form of the Rune
   *   p yields ~p
   *  ~p yields ~~p
   * @returns {Rune}
   */
  Rune.prototype.negate = function () {
    return new Rune(this.copy(), undefined, Bindings.CHAOS);
  };

  /**
   * Returns the inverted form of the Rune
   *   p yields ~p
   *  ~p yields p
   * @returns {Rune}
   */
  Rune.prototype.inverse = function () {
    return (this.op === Bindings.CHAOS) ? this.arg0.copy() : this.negate();
  };

  /**
   * Create a new Rune based on copies of arg0 and arg1.
   * @param {Rune} arg0 The first Rune to copy
   * @param {Rune} arg1 The second Rune to copy
   * @param {Binding} op The Binding to use
   * @returns {Rune} A Rune crafted with copies of arg0 and arg1
   */
  Rune.fromComponents = function (arg0, arg1, op) {
    var newArg0 = (arg0) ? arg0.copy() : undefined;
    var newArg1 = (arg1) ? arg1.copy() : undefined;
    return new Rune(newArg0, newArg1, op);
  };

  /**
   * Creates a Rune from the provided String. Assumes the format is correct and
   * offers no error checking.
   * @param {String} data The String to parse
   * @returns {Rune} The Rune structure representing the provided data
   */
  Rune.fromString = function (data) {
    // Init
    var current = "";
    var subRuneString;
    var index = 0;
    var endex = data.length - 1;

    // Get the next character
    current = data[index];

    // Case 1: Glyph
    var tryGlyph = Glyphs.fromCharacter(current);
    if (tryGlyph !== undefined) {
      return new Rune(tryGlyph);
    }
    // Case 2: (Rune Binding Rune)
    else if (current === "(") {
      // Skip the paren
      index++;

      // Find the midpoint
      // The midpoint is the operator that splits arg0 from arg1
      // Since arg0 and arg1 are Facts, they may have Operators of their own, 
      // so we can't simply get the next Operator.
      var midpoint = index;
      var parenCount = 0;
      for (var search = index; search <= endex; search++) {
        // Reset current
        current = data[search];

        // Check if current is a paren
        var tryOp = Bindings.fromCharacter(current);
        if (current === "(") {
          parenCount++;
        } else if (current === ")") {
          parenCount--;
        } else if (parenCount === 0 &&
            tryOp !== undefined &&
            !tryOp.equals(Bindings.CHAOS)) {
          midpoint = search;
          break;
        }
      }

      // Get arg0
      var arg0;
      subRuneString = data.substring(index, midpoint);
      arg0 = this.fromString(subRuneString);

      // Get the Binding
      var op = Bindings.fromCharacter(data[midpoint]);

      // Get arg1
      var arg1;
      subRuneString = data.substring(midpoint + 1, endex);
      arg1 = this.fromString(subRuneString);

      // Create the Rune
      return new Rune(arg0, arg1, op);
    }
    // Case 3: ~(Rune)
    else if (Bindings.fromCharacter(current).equals(Bindings.CHAOS) &&
        data[index + 1] === "(") {
      // Move up a char
      index++;

      // Get the inner Rune
      subRuneString = data.substring(index + 1, data.lastIndexOf(")"));

      // Negate the inner Rune
      return new Rune(this.fromString(subRuneString), undefined, Bindings.CHAOS);
    }
  };

  return Rune;
});