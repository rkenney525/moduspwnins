define(['jquery', 'underscore'], function ($, _) {
  /**
   * Create a Binding instance with a particular style and internal representation.
   * @param {String} displayClass The CSS class used to style the binding
   * @param {String} character The character used for internal representation
   * @returns {Binding} A Binding instance
   */
  var Binding = function (displayClass, character) {
    this.displayClass = displayClass;
    this.character = character;
  };
  
  /**
   * The template used to render HTML
   */
  Binding.template = _.template($('#bindingTemplate').html());
  
  /**
   * Compares the invoking Binding to 'other'. This does not check against
   * internal representation. It must be the exact instance.
   * @param {Binding} other The Binding to compare to.
   * @returns {Boolean} True if this instance is exactly the other one, false otherwise
   */
  Binding.prototype.equals = function (other) {
    return this === other;
  };

  Binding.prototype.html = function (arg0, arg1, stacked) {
    return Binding.template({
      displayClass: this.displayClass,
      arg0: arg0,
      arg1: arg1,
      halfClass: stacked ? 'binding-half-stack' : 'binding-half-side'
    }).trim();
  };

  return Binding;
});