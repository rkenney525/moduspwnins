define(['Spell', 'Bindings', 'Rune'], function (Spell, Bindings, Rune) {
  // TODO replace apply method with generic one that uses forms
  var Spells = {
    // Modus Ponens
    AE: new Spell('Aerial Extraction', Spell.Type.GENERATION,
            [new Spell.Form('B', ['(A>B)', 'A'])],
            function (arg0, arg1) {
              // Init
              var results = [];

              // Sanity check op
              if (arg0.op !== Bindings.AIR) {
                return results;
              }

              /* For example:
               *  arg0 = (p>q)
               *  arg1 = p
               * Then:
               *  arg0.arg0 = p
               * And since:
               *  arg0.arg0 == arg1 (p == p)
               * Return:
               *  arg0.arg1 (q)
               */
              if (arg0.arg0.equals(arg1)) {
                results.push(arg0.arg1.copy());
              }

              return results;
            }),
    // Modus Tollens
    AC: new Spell('Aerial Confusion', Spell.Type.GENERATION,
            [new Spell.Form('~(A)', ['(A>B)', '~(B)'])],
            function (arg0, arg1) {
              // Init
              var results = [];

              // Sanity check arg0
              if (arg0.op !== Bindings.AIR) {
                return results;
              }

              // Sanity check arg1
              if (arg1.op !== Bindings.CHAOS) {
                return results;
              }

              /* For example:
               *  arg0 = (p>q)
               *  arg1 = ~(q)
               * Then:
               *  arg0.arg1 = q
               *  neg(arg0.arg0) = ~(q)
               * And since:
               *  neg(arg0.arg1) == arg1 (~(q) == ~(q))
               * Return:
               *  arg0.arg1 ~(p)
               */
              var negQ = arg0.arg1.negate();
              if (negQ.equals(arg1)) {
                results.push(arg0.arg0.negate());
              }

              return results;
            }),
    // Disjunctive Syllogism
    FS: new Spell('Flame Seperation', Spell.Type.GENERATION,
            [new Spell.Form('B', ['(A|B)', '~(A)'])],
            function (arg0, arg1) {
              // Init
              var results = [];

              // Sanity check arg0
              if (arg0.op !== Bindings.FIRE) {
                return results;
              }

              // Sanity check arg1
              if (arg1.op !== Bindings.CHAOS) {
                return results;
              }

              /* For example:
               *  arg0 = (p|q)
               *  arg1 = ~(p)
               * Then:
               *  neg(arg0.arg0) = ~(p)
               * And since:
               *  neg(arg0.arg0) == arg1 (~(p) == ~(p))
               * Return:
               *  arg0.arg1
               */
              if (arg0.arg0.negate().equals(arg1)) {
                results.push(arg0.arg1.copy());
              }

              return results;
            }),
    // Constructive Dilemma
    TD: new Spell('Torrential Downpour', Spell.Type.GENERATION,
            [new Spell.Form('(B|D)', ['((A>B)&(C>D))', '(A|C)'])],
            function (arg0, arg1) {
              // Init
              var results = [];

              // Sanity check arg0
              if (arg0.op !== Bindings.WATER ||
                      arg0.arg0.op !== Bindings.AIR ||
                      arg0.arg1.op !== Bindings.AIR) {
                return results;
              }

              // Sanity check arg1
              if (arg1.op !== Bindings.FIRE) {
                return results;
              }

              /* For example: 
               *  arg0 = ((p>q)&(r>s))
               *  arg1 = (p|r)
               * Then:
               *  arg0.arg0 = (p>q)
               *  arg0.arg0.arg0 = p
               *  arg0.arg0.arg1 = q
               *  arg0.arg1 = (r>s)
               *  arg0.arg1.arg0 = r
               *  arg0.arg1.arg1 = s
               *  arg1.arg0 = p
               *  arg1.arg1 = r
               * And since:
               *  arg0.arg0.arg0 == arg1.arg0 (p == p)
               *  arg0.arg1.arg0 == arg1.arg1 (r == r)
               * Return:
               *  (arg0.arg0.arg1 | arg0.arg1.arg1) (q|s)
               */
              if (arg0.arg0.arg0.equals(arg1.arg0) &&
                      arg0.arg1.arg0.equals(arg1.arg1)) {
                results.push(Rune.fromComponents(arg0.arg0.arg1, arg0.arg1.arg1, Bindings.FIRE));
              }

              return results;
            }),
    // Hypothetical Syllogism
    AR: new Spell('Aerial Rotation', Spell.Type.GENERATION,
            [new Spell.Form('(A>C)', ['(A>B)', '(B>C)'])],
            function (arg0, arg1) {
              // Init
              var results = [];

              // Sanity check arg0
              if (arg0.op !== Bindings.AIR) {
                return results;
              }

              // Sanity check arg1
              if (arg1.op !== Bindings.AIR) {
                return results;
              }

              /* For example:
               *  arg0 = (p>q)
               *  arg1 = (q>r)
               * Then:
               *  arg0.arg0 = p
               *  arg0.arg1 = q
               *  arg1.arg0 = q
               *  arg1.arg1 = r
               * And since:
               *  arg0.arg1 == arg1.arg0 (q == q)
               * Return:
               *  (arg0.arg0 > arg1.arg1)
               */
              if (arg0.arg1.equals(arg1.arg0)) {
                results.push(Rune.fromComponents(arg0.arg0, arg1.arg1, Bindings.AIR));
              }

              return results;
            }),
    // Simplification
    ER: new Spell('Erosion', Spell.Type.GENERATION,
            [new Spell.Form('A', ['(A&B)'])],
            function (arg0) {
              // Init
              var results = [];

              // Sanity check arg0
              if (arg0.op !== Bindings.WATER) {
                return results;
              }

              /* For example:
               *  arg0 = (p&q)
               * Then:
               *  arg0.arg0 = p
               * So return:
               *  arg0.arg0 (p)
               */
              results.push(arg0.arg0.copy());
              return results;
            }),
    // Conjunction
    AQ: new Spell('Aquajunction', Spell.Type.GENERATION,
            [new Spell.Form('(A&B)', ['A', 'B'])],
            function (arg0, arg1) {
              // Init
              var results = [];

              /* For example:
               *  arg0 = p
               *  arg1 = q
               * So return:
               *  (arg0 & arg1) (p&q)
               */
              results.push(Rune.fromComponents(arg0, arg1, Bindings.WATER));
              return results;
            }),
    // Absorption
    DI: new Spell('Diffusion', Spell.Type.GENERATION,
            [new Spell.Form('(A>B)', ['(A>(A&B))']),
              new Spell.Form('(A>(A&B))', ['(A>B)'])],
            function (arg0) {
              // Init
              var results = [];

              // arg0 must be a conditional.  If it is, then the rule can be either 
              // applied or reversed, both will be shown.
              if (arg0.op === Bindings.AIR) {
                /* For example (application):
                 *  arg0 = (p>(p&q))
                 * Then:
                 *  arg0.arg0 = p
                 *  arg0.arg1.arg0 = p
                 * And since:
                 *  arg0.arg0 == arg0.arg1.arg0 (p == p)
                 * Return:
                 *  (arg0.arg0 > arg0.arg1.arg1)
                 */
                if (arg0.arg0.equals(arg0.arg1.arg0)) {
                  results.push(Rune.fromComponents(arg0.arg0, arg0.arg1.arg1, Bindings.AIR));
                }
                /* For example (reverse application):
                 *  arg0 = (p>q)
                 * Then:
                 *  arg0.arg0 = p
                 *  arg0.arg1 = q
                 * And we want:
                 *  (p>(p&q))
                 * So return:
                 *  (arg0.arg0 > (arg0.arg0 & arg0.arg1))
                 */
                var conj = Rune.fromComponents(arg0.arg0, arg0.arg1, Bindings.WATER);
                results.push(Rune.fromComponents(arg0.arg0, conj, Bindings.AIR));
              }

              return results;
            }),
    // Addition
    BF: new Spell('Birth by Flame', Spell.Type.GENERATION,
            [new Spell.Form('(A|B)', ['A'])],
            function (arg0, arg1) {
              // Init
              var results = [];

              /* For example:
               *  arg0 = p
               *  arg1 = [Something from the user] (q)
               * So return:
               *  (arg0 | arg1) (p | q)
               */
              results.push(Rune.fromComponents(arg0, arg1, Bindings.FIRE));

              return results;
            }),
    // DeMorgan's Law
    CI: new Spell('Chaotic Inverse', Spell.Type.MANIPULATION,
            [new Spell.Form('~((A&B))', ['(~(A)|~(B))']),
              new Spell.Form('(~(A)|~(B))', ['~((A&B))']),
              new Spell.Form('~((A|B))', ['(~(A)&~(B))']),
              new Spell.Form('(~(A)&~(B))', ['~((A|B))'])],
            function (arg0) {
              // Init
              var results = [];

              // Init
              var op = arg0.op;
              function getOtherOp(op) {
                if (op === Bindings.WATER) {
                  return Bindings.FIRE;
                } else if (op === Bindings.FIRE) {
                  return Bindings.WATER;
                } else {
                  return null;
                }
              }

              // Sanity check
              if (op === Bindings.CHAOS) {
                /* For example:
                 *  arg0 = ~(p op q)
                 * Return:
                 *  (~p otherOp ~q)
                 */
                if (arg0.arg0.op === Bindings.WATER ||
                        arg0.arg0.op === Bindings.FIRE) {
                  results.push(Rune.fromComponents(
                          arg0.arg0.arg0.inverse(),
                          arg0.arg0.arg1.inverse(),
                          getOtherOp(arg0.arg0.op)));
                }
              } else if (op === Bindings.WATER ||
                      op === Bindings.FIRE) {
                /* For example:
                 *  arg0 = ~p op ~q
                 * Return:
                 *  ~(p otherOp q)
                 */
                results.push(Rune.fromComponents(arg0.arg0.inverse(), arg0.arg1.inverse(), getOtherOp(op)).negate());
              }

              // Return the result
              return results;
            }),
    // Commutation
    MI: new Spell('Miraga', Spell.Type.MANIPULATION,
            [new Spell.Form('(A|B)', ['(B|A)']),
              new Spell.Form('(A&B)', ['(B&A)'])],
            function (arg0) {
              // Init
              var results = [];

              /* For example (same logic for &)
               *  arg0 = (p | q)
               * Then
               *  arg0.arg0 = p
               *  arg0.arg1 = q
               * So return
               *  (arg0.arg1 | arg0.arg0) (q | p)
               */
              if (arg0.op === Bindings.FIRE ||
                      arg0.op === Bindings.WATER) {
                results.push(Rune.fromComponents(arg0.arg1, arg0.arg0, arg0.op));
              }

              return results;
            }),
    // Association
    TR: new Spell('Transcendence', Spell.Type.MANIPULATION,
            [new Spell.Form('(A|(B|C))', ['((A|B)|C)']),
              new Spell.Form('((A|B)|C)', ['(A|(B|C))']),
              new Spell.Form('(A&(B&C))', ['((A&B)&C)']),
              new Spell.Form('((A&B)&C)', ['(A&(B&C))'])],
            function (arg0) {
              // Init
              var results = [];
              var op = arg0.op;

              // Sanity check
              if (op !== Bindings.FIRE &&
                      op !== Bindings.WATER) {
                return results;
              }

              /* In the interest of space, I'm going to avoid my verbose comment style
               * Association can be ambiguous, so first here's an unambiguous case
               *  p|(q|r) -> returns (p|q)|r
               * Note this applies only to & and | and the op has to be the op in all 
               * parties. Now the ambiguous case
               *  (p|q)|(r|s) -> returns p|(q|(r|s)) AND ((p|q)|r)|s
               * So we return both. We accomplish this by checking operator of arg0 
               * and arg1 independently, and if they match then add the result
               */
              // Check arg0.arg0
              if (op === arg0.arg0.op) {
                results.push(Rune.fromComponents(arg0.arg0.arg0,
                        Rune.fromComponents(arg0.arg0.arg1, arg0.arg1, op), op));
              }

              // Check arg0.arg1
              if (op === arg0.arg1.op) {
                results.push(Rune.fromComponents(
                        Rune.fromComponents(arg0.arg0, arg0.arg1.arg0, op),
                        arg0.arg1.arg1,
                        op));
              }

              // Return the result
              return results;
            }),
    // Distribution
    ST: new Spell('Steamify', Spell.Type.MANIPULATION,
            [new Spell.Form('(A|(B&C))', ['((A|B)&(A|C))']),
              new Spell.Form('((A|B)&(A|C))', ['(A|(B&C))']),
              new Spell.Form('(A&(B|C))', ['((A&B)|(A&C))']),
              new Spell.Form('((A&B)|(A&C))', ['(A&(B|C))'])],
            function (arg0) {
              var results = [];
              var o1 = arg0.op;
              var o2;

              // Sanity check
              if (arg0.op !== Bindings.FIRE &&
                      arg0.op !== Bindings.WATER) {
                return results;
              }

              /* Case 1
               *  o1, o2 = | or & but both cant be the same
               *  arg0 = (p o1 (q o2 r))
               * Return
               *  ((p o1 q) o2 (p o1 r))
               */
              // Make sure o2 is a valid operator and that it isnt the same as o1
              if (arg0.op !== arg0.arg1.op &&
                      (arg0.arg1.op === Bindings.FIRE ||
                              arg0.arg1.op === Bindings.WATER)) {
                o2 = arg0.arg1.op;
                results.push(Rune.fromComponents(
                        Rune.fromComponents(arg0.arg0, arg0.arg1.arg0, o1),
                        Rune.fromComponents(arg0.arg0, arg0.arg1.arg1, o1),
                        o2
                        ));
              }
              /* Case 2
               *  o1, o2 = | or & but both cant be the same
               *  arg0 = ((p o2 q) o1 (p o2 r))
               * Return
               *  (p o2 (q o1 r))
               */
              // Make sure o2 is a valid operator, it isnt the same as o1, both 
              // arg0.arg0 AND arg0.arg1 are using o2, and that the first arg is the 
              // same for arg0.arg0 and arg0.arg1
              if (arg0.op !== arg0.arg0.op &&
                      (arg0.arg0.op === Bindings.FIRE ||
                              arg0.arg0.op === Bindings.WATER) &&
                      arg0.arg0.op === arg0.arg1.op &&
                      arg0.arg0.arg0.equals(arg0.arg1.arg0)) {
                o2 = arg0.arg0.op;
                results.push(Rune.fromComponents(
                        arg0.arg0.arg0,
                        Rune.fromComponents(arg0.arg0.arg1, arg0.arg1.arg1, o1),
                        o2));
              }

              // Return the results
              return results;
            }),
    // Double Negation
    OC: new Spell('Ordered Chaos', Spell.Type.MANIPULATION,
            [new Spell.Form('A', ['~(~(A))']),
              new Spell.Form('~(~(A))', ['A'])],
            function (arg0) {
              var results = [];
              /* For example type #1:
               *  arg0 = ~(~p)
               * Then:
               *  arg0.arg0.arg0 = p
               * So return:
               *  arg0.argo.arg0 (p)
               */
              if (arg0.op === Bindings.CHAOS &&
                      arg0.arg0.op === Bindings.CHAOS) {
                results.push(arg0.arg0.arg0.copy());
              }
              /* For example type #2:
               *  arg0 = p
               * Then:
               *  neg(neg(arg0)) = ~(~(p))
               * So return:
               *  neg(neg(arg0)) ( ~(~(p)) )
               */
              // Always do this one
              // TODO consider making this an else so it doesnt get too chaotic
              results.push(arg0.negate().negate());

              // Return the results
              return results;
            }),
    // Transposition
    DW: new Spell('Dark Wind', Spell.Type.MANIPULATION,
            [new Spell.Form('(A>B)', ['(~(B)>~(A))']),
              new Spell.Form('(~(B)>~(A))', ['(A>B)']),
              new Spell.Form('(~(A)>B)', ['(~(B)>A)']),
              new Spell.Form('(A>~(B))', ['(B>~(A))'])],
            function (arg0) {
              // Init
              var results = [];

              // Sanity check
              if (arg0.op !== Bindings.AIR) {
                return results;
              }

              /**
               * The rule:
               *  p>q  <-> ~q>~p
               *  ~p>q <-> ~q>p
               */
              results.push(Rune.fromComponents(arg0.arg1.inverse(), arg0.arg0.inverse(), Bindings.AIR));

              return results;
            }),
    // Material Implication
    CO: new Spell('Condensate', Spell.Type.MANIPULATION,
            [new Spell.Form('(A>B)', ['(~(A)|B)']),
              new Spell.Form('(~(B)|A)', ['(A>B)']),
              new Spell.Form('(~(A)>B)', ['(A|B)'])],
            function (arg0) {
              // Init
              var results = [];

              if (arg0.op === Bindings.AIR) {
                /*
                 * (p>q) <-> (~p|q)
                 * (~p>q) <-> (p|q)
                 */
                results.push(Rune.fromComponents(arg0.arg0.inverse(), arg0.arg1, Bindings.FIRE));
              } else if (arg0.op === Bindings.FIRE) {
                /*
                 * (p|q) <-> (~p>q)
                 * (~p|q) <-> (p>q)
                 */
                results.push(Rune.fromComponents(arg0.arg0.inverse(), arg0.arg1, Bindings.AIR));
              }

              // Return
              return results;
            }),
    // Material Equivalence
    TE: new Spell('Terraformation', Spell.Type.MANIPULATION,
            [new Spell.Form('((A>B)&(B>A))', ['(A%B)']),
              new Spell.Form('((A&B)|(~(A)&~(B)))', ['(A%B)']),
              new Spell.Form('(A%B)', ['((A>B)&(B>A))']),
              new Spell.Form('(A%B)', ['((A&B)|(~(A)&~(B)))'])],
            function (arg0) {
              // Init
              var op = arg0.op;
              var results = [];

              if (op === Bindings.EARTH) {
                /*
                 * Form of p%q
                 */
                var part1, part2;

                // First result is ( (p>q) & (q>p) )
                part1 = Rune.fromComponents(arg0.arg0, arg0.arg1, Bindings.AIR);
                part2 = Rune.fromComponents(arg0.arg1, arg0.arg0, Bindings.AIR);
                results.push(Rune.fromComponents(part1, part2, Bindings.WATER));

                // Second result is ( (p&q) | (~p&~q))
                part1 = Rune.fromComponents(arg0.arg0, arg0.arg1, Bindings.WATER);
                part2 = Rune.fromComponents(arg0.arg0.negate(), arg0.arg1.negate(), Bindings.WATER);
                results.push(Rune.fromComponents(part1, part2, Bindings.FIRE));
              } else if (op === Bindings.WATER) {
                /*
                 * Form of ( (p>q) & (q>p) )
                 */
                // Check inner args
                if (arg0.arg0.op === Bindings.AIR &&
                        arg0.arg1.op === Bindings.AIR) {
                  if (arg0.arg0.arg0.equals(arg0.arg1.arg1) &&
                          arg0.arg0.arg1.equals(arg0.arg1.arg0)) {
                    // Create (p%q)
                    results.push(Rune.fromComponents(arg0.arg0.arg0, arg0.arg0.arg1, Bindings.EARTH));
                  }
                }
              } else if (op === Bindings.FIRE) {
                /*
                 * Form of ( (p&q) | (~p&~q))
                 */
                // Check inner args
                if (arg0.arg0.op === Bindings.WATER &&
                        arg0.arg1.op === Bindings.WATER) {
                  if (arg0.arg0.arg0.negate().equals(arg0.arg1.arg0) &&
                          arg0.arg0.arg1.negate().equals(arg0.arg1.arg1)) {
                    // Create (p%q)
                    results.push(Rune.fromComponents(arg0.arg0.arg0, arg0.arg0.arg1, Bindings.EARTH));
                  }
                }
              }

              // Return
              return results;
            }),
    // Exportation
    TC: new Spell('Transformative Currents', Spell.Type.MANIPULATION,
            [new Spell.Form('((A&B)>C)', ['(A>(B>C))']),
              new Spell.Form('(A>(B>C))', ['((A&B)>C)'])],
            function (arg0) {
              // Init
              var results = [];

              if (arg0.op === Bindings.AIR) {
                /*
                 * Form of ( (p & q) > r )
                 */
                if (arg0.arg0.op === Bindings.WATER) {
                  var newCond = Rune.fromComponents(arg0.arg0.arg1, arg0.arg1, Bindings.AIR);
                  results.push(Rune.fromComponents(arg0.arg0.arg0, newCond, Bindings.AIR));
                }
                /*
                 * Form of ( p > (q > r) )
                 */
                if (arg0.arg1.op === Bindings.AIR) {
                  var newConj = Rune.fromComponents(arg0.arg0, arg0.arg1.arg0, Bindings.WATER);
                  results.push(Rune.fromComponents(newConj, arg0.arg1.arg1, Bindings.AIR));
                }
              }

              // Return
              return results;
            }),
    // Tautology
    FR: new Spell('Fiery Reducto', Spell.Type.MANIPULATION,
            [new Spell.Form('(A|A)', ['A']),
              new Spell.Form('A', ['(A|A)'])],
            function (arg0) {
              var results = [];
              /* Rule:
               *  (p|p) <-> p
               */
              // If you can do the shortening version, do it
              if (arg0.op === Bindings.FIRE &&
                      arg0.arg0.equals(arg0.arg1)) {
                results.push(arg0.arg0.copy());
              }
              // This version can always be applied, but isn't always useful
              results.push(Rune.fromComponents(arg0, arg0, Bindings.FIRE));

              // Return the results
              return results;
            }),
    // Process of Elimination
    GL: new Spell('Gift of Life', Spell.Type.GENERATION,
            [new Spell.Form('(A#B)', ['~(A)', 'B']),
              new Spell.Form('(A#B)', ['A', '~(B)'])],
            function (arg0, arg1) {
              // Init
              var results = [];

              // Sanity check arg0
              if (arg0.op !== Bindings.LIFE) {
                return results;
              }

              /* For example type #1:
               *  arg0 = (p # q)
               *  arg1 = ~(p)
               * Then:
               *  arg0.arg0 = p
               *  arg0.arg1 = q
               *  neg(arg0.arg0) = ~(p)
               * And since:
               *  neg(arg0.arg0) == arg1 (~(p) == ~(p))
               * Return:
               *  arg0.arg1 (q)
               */
              if (arg1.op === Bindings.CHAOS) {
                var negp = arg0.arg0.negate();
                if (negp.equals(arg1)) {
                  results.push(arg0.arg1.copy());
                }
              } else {
                /* For example type #2:
                 *  arg0 = (p # q)
                 *  arg1 = p
                 * Then:
                 *  arg0.arg0 = p
                 *  arg0.arg1 = q
                 *  neg(arg0.arg1) = ~(q)
                 * And since:
                 *  arg0.arg0 == arg1 (p == p)
                 * Return:
                 *  neg(arg0.arg1) (~(q))
                 */
                if (arg0.arg0.equals(arg1)) {
                  results.push(arg0.arg1.negate());
                }
              }

              return results;
            })
  };
  /**
   * Check if Spell is unary.
   * 
   * @param {Spell} spell The Spell to check
   * @returns {boolean} True if spell takes only one operand, false otherwise
   */
  Spells.isUnary = function (spell) {
    return (spell === Spells.ER) ||
            (spell === Spells.DI) ||
            (spell === Spells.CI) ||
            (spell === Spells.MI) ||
            (spell === Spells.TR) ||
            (spell === Spells.ST) ||
            (spell === Spells.OC) ||
            (spell === Spells.DW) ||
            (spell === Spells.CO) ||
            (spell === Spells.TE) ||
            (spell === Spells.TC) ||
            (spell === Spells.FR);
  };

  return Spells;
});