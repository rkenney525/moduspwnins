define(['jquery', 'underscore', 'Scene', 'Rune', 'Pager', 'Spells'],
        function ($, _, Scene, Rune, Pager, Spells) {
          return Scene.extend({
            SELECTED_CLASS: 'selected-rune',
            FADE_TIME: 500,
            template: _.template($('#levelExecutionMainTemplate').html()),
            argHelperTemplate: _.template($('#levelExecutionArgHelperTemplate').html()),
            spellControlTemplate: _.template($('#levelExecutionSpellControlTemplate').html()),
            spellDescriptionTemplate: _.template($('#levelExecutionSpellDescriptionTemplate').html()),
            backdrop: 'level-execution-backdrop',
            events: {
              'click #RunePagerNext': 'runePagerNext',
              'click #RunePagerPrev': 'runePagerPrev',
              'click #SpellBookRightPage>.spell-pager-control': 'spellPagerNext',
              'click #SpellBookLeftPage>.spell-pager-control': 'spellPagerPrev',
              'click .selectable-rune': 'selectRune',
              'click .spell-argument': 'engageArgument',
              'click #Result': 'engageResult'
            },
            /**
             * Displays all components of the Scene and sets up rune and spell paging
             * @param {Object} data Contains information regarding the current level
             * @returns {undefined}
             */
            start: function (data) {
              _.bindAll(this, 'displayRunes', 'displaySpell');
              this.$stage.html(this.template());
              
              // Get the Runes
              this.runes = data.runes;
              this.runePager = new Pager(this.runes, 7, this.displayRunes,
                      $('#RunePagerPrev'),
                      $('#RunePagerNext'));
              this.runePager.display();

              // Get the Spells
              this.spells = data.spells;
              this.spellPager = new Pager(this.spells, 1, this.displaySpell,
                      $('#SpellBookLeftPage').children('.spell-pager-control'),
                      $('#SpellBookRightPage').children('.spell-pager-control'));
              this.spellPager.display();
            },
            /**
             * Go to the next page of Runes and display them
             * @returns {undefined}
             */
            runePagerNext: function () {
              this.runePager.nextAndDisplay();
            },
            /**
             * Go to the previous page of Runes and display them
             * @returns {undefined}
             */
            runePagerPrev: function () {
              this.runePager.prevAndDisplay();
            },
            /**
             * Go to the next page of Spells (one) and display them
             * @returns {undefined}
             */
            spellPagerNext: function () {
              this.spellPager.nextAndDisplay();
            },
            /**
             * Go to the previous page of Spells (one) and display them
             * @returns {undefined}
             */
            spellPagerPrev: function () {
              this.spellPager.prevAndDisplay();
            },
            /**
             * Display runes in the Rune container. Should not be called directly;
             * only to be called by the Pager.
             * @param {Array[Rune]} runes The Runes to display
             * @returns {undefined}
             */
            displayRunes: function (runes) {
              var $container = $('#RuneSlabContainer');
              $container.empty();
              for (var i = 0; i < runes.length; i++) {
                $container.append($(runes[i].html()).addClass('selectable-rune'));
              }
            },
            /**
             * Display current Spell information. Should not be called directly;
             * only to be called by the Pager.
             * @param {Array[Spell]} spells The spells (one) to display
             * @returns {undefined}
             */
            displaySpell: function (spells) {
              var spell = spells[0];
              var $leftPageContainer = $('#SpellBookLeftPage').children('.spell-description-contents');
              var $rightPageContainer = $('#SpellBookRightPage').children('.spell-control-contents');
              var isUnary = Spells.isUnary(spell);
              $leftPageContainer.html(this.spellDescriptionTemplate({
                name: spell.name,
                isUnary: isUnary,
                forms: spell.forms
              }));
              $rightPageContainer.html(this.spellControlTemplate({
                isUnary: isUnary
              }));
            },
            /**
             * Selects a particular Rune. This deselects any other previously
             * selected Rune.
             * @param {Object} event The click event
             * @returns {undefined}
             */
            selectRune: function (event) {
              $('.' + this.SELECTED_CLASS).removeClass(this.SELECTED_CLASS);
              $(event.currentTarget).addClass(this.SELECTED_CLASS);
              this.addHelpers();
            },
            /**
             * Handles the click of an argument space. Either places the
             * selected Rune in that location, clears the contents if no Rune
             * is selected but the space is populated, otherwise does nothing.
             * @param {Object} event The click event
             * @returns {undefined}
             */
            engageArgument: function (event) {
              var $selected = $('.' + this.SELECTED_CLASS);
              var $arg = $(event.currentTarget);
              if ($selected.size() > 0) {
                $arg.html(Rune.fromString($selected.data('recipe')).html());
                $selected.removeClass(this.SELECTED_CLASS);
                this.removeHelpers();
                this.applySpell();
              } else {
                $arg.children().fadeOut(this.FADE_TIME, function () {
                  $arg.empty();
                });
              }
            },
            /**
             * If there is a result present, it is added to the list of Runes.
             * @returns {undefined}
             */
            engageResult: function () {
              var $result = $('#Result');
              var runeString = $result.children().data('recipe');
              if (runeString) {
                this.runes.push(Rune.fromString(runeString));
                this.runePager.display();
                $result.empty();
              }
            },
            /**
             * Add helper text to empty Rune argument slots.
             * @returns {undefined}
             */
            addHelpers: function () {
              var helperTemplate = this.argHelperTemplate;
              _.each($('.spell-argument'), function (arg) {
                var $arg = $(arg);
                if ($arg.children().size() === 0) {
                  $arg.html(helperTemplate());
                }
              });
            },
            /**
             * Remove helper text from Rune argument slots
             * @returns {undefined}
             */
            removeHelpers: function () {
              $('.arg-helper').remove();
            },
            /**
             * Apply the current Spell on the current arguments. Does nothing if
             * given incomplete data or the data does not match the Spell's
             * forms. If there is a result, puts it in the result box.
             * @returns {undefined}
             */
            applySpell: function () {
              function getRuneFromDiv(div) {
                var str = $(div).children().data('recipe');
                return (str) ? Rune.fromString(str) : undefined;
              }
              var $args = $('.spell-argument');
              var arg0 = getRuneFromDiv($args[0]);
              var arg1 = ($args.size() > 1) ? getRuneFromDiv($args[1]) : undefined;
              var spell = this.spells[this.spellPager.currentPage - 1];
              var result = spell.apply(arg0, arg1);
              if (result.length > 0) {
                // TODO handle multiple responses
                $('#Result').html(result[0].html());
              }
            }
          });
        });