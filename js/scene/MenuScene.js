define(['jquery', 'underscore', 'Scene', 'PickLevelScene', 'Glyphs', 'LevelExecutionScene', 'Rune', 'Spells'],
    function ($, _, Scene, PickLevelScene, Glyphs, LevelExecutionScene, Rune, Spells) {
      return Scene.extend({
        GLYPH_TIME_DELAY_MS: 3000,
        TITLE_TIME_DELAY_MS: 7000,
        template: _.template($('#mainMenuTemplate').html()),
        glyphTemplate: _.template($('#mainMenuDancingGlyphTemplate').html()),
        backdrop: 'menu-backdrop',
        events: {
          'click #PlayGame': 'continueGame',
          'click #PickLevel': 'pickLevel',
          'click #Options': 'test',
          'click #ExitGame': 'exit'
        },
        /**
         * The function called when the Scene begins
         * @param {Object} data All configuration options passed to the scene
         * @returns {undefined}
         */
        start: function (data) {
          // Bind createDancingGlyphs since it is a callback in setTimeout
          _.bindAll(this, 'createDancingGlyphs');

          // Set up the stage with visual effects
          this.$stage.html(this.template());
          setTimeout(this.titleAnimation, this.TITLE_TIME_DELAY_MS);
          this.createDancingGlyphs(Glyphs.values());
        },
        continueGame: function () {
          // TODO Load data from some permanent means
          var runes = [];
          for (var i = 0; i < 7; i++) {
            runes.push(Rune.fromString('((A&D)>(B|C))'));
          }
          for (var i = 7; i < 14; i++) {
            runes.push(Rune.fromString('(I#~(G))'));
          }
          for (var i = 14; i < 20; i++) {
            runes.push(Rune.fromString('~(~(~(C)))'));
          }
          runes.push(Rune.fromString('(A%(B|C))'));
          runes.push(Rune.fromString('(A>B)'));
          runes.push(Rune.fromString('A'));
          var spells = [Spells.AE, Spells.DI, Spells.CI, Spells.FS, Spells.TD, Spells.AR];
          this.startScene(LevelExecutionScene, {
            runes: runes,
            spells: spells
          });
        },
        test: function () {
          alert('dees');
        },
        /**
         * Switch to the PickLevel Scene
         * @returns {undefined}
         */
        pickLevel: function () {
          this.startScene(PickLevelScene);
        },
        /**
         * Run the title animation, which consists of slowly fading in 'Pwnins'
         * @returns {undefined}
         */
        titleAnimation: function () {
          $('.menu-title-pwnin').addClass('menu-title-pwnin-active');
        },
        /**
         * Recursively create all glyphs in the list provided. Glyph animation is done via CSS.
         * @param {Array[Glyph]} glyphs An array of glyphs to animate on the screen
         * @returns {undefined}
         */
        createDancingGlyphs: function (glyphs) {
          if (glyphs.length > 0) {
            var glyph = glyphs.pop();
            $('#OpeningGraphic').append(this.glyphTemplate({
              character: glyph.character
            }));
            setTimeout(this.createDancingGlyphs, this.GLYPH_TIME_DELAY_MS, glyphs);
          }
        },
        /**
         * Kills the game
         * @returns {undefined}
         */
        exit: function () {
          window.close();
        }
      });
    });