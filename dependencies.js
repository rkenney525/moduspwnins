requirejs.config({
    baseUrl: '.',
    paths: {
        jquery: 'js/lib/jquery-1.9.0/jquery.min',
        blockUI: 'js/lib/jquery-blockui/jquery.blockUI',
        underscore: 'js/lib/underscore.js/underscore-min',
        // Arch
        SceneManager: 'js/arch/SceneManager',
        Scene: 'js/arch/Scene',
        // Modules
        Glyph: 'js/module/Glyph',
        Glyphs: 'js/module/Glyphs',
        Binding: 'js/module/Binding',
        Bindings: 'js/module/Bindings',
        Rune: 'js/module/Rune',
        Spell: 'js/module/Spell',
        Spells: 'js/module/Spells',
        Pager: 'js/module/Pager',
        // Scenes
        MenuScene: 'js/scene/MenuScene',
        PickLevelScene: 'js/scene/PickLevelScene',
        LevelExecutionScene: 'js/scene/LevelExecutionScene'
    }
});