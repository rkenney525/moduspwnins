module.exports = function (grunt) {
  // Reusable configurations
  var stagingLocation = 'build/stage/';
  var requireConfig = function (optimize) {
    return {
      options: {
        baseUrl: '.',
        mainConfigFile: 'dependencies.js',
        name: 'js/moduspwnins.js',
        out: stagingLocation + 'moduspwnins.js',
        optimize: (optimize) ? 'uglify' : 'none'
      }
    };
  };

  // Config
  grunt.initConfig({
    clean: ['build'],
    preprocess: {
      aggregate: {
        src: 'moduspwnins.html',
        dest: stagingLocation + 'moduspwnins.html'
      }
    },
    requirejs: {
      prod: requireConfig(true),
      dev: requireConfig(false)
    },
    cssmin: {
      target: {
        files: {
          'build/stage/moduspwnins.css': ['./css/**/*.css']
        }
      }
    },
    copy: {
      require: {
        flatten: true,
        src: 'js/lib/require.js/require.js',
        dest: stagingLocation,
        expand: true
      },
      img: {
        src: 'img/**',
        dest: stagingLocation
      },
      font:{
        src: 'fonts/**',
        dest: stagingLocation
      }
    },
    nwjs: {
      options: {
        platforms: ['win'],
        buildDir: 'build',
        version: 'v0.12.0'
      },
      src: ['./package.json', stagingLocation + '**/*']
    },
    mochaTest: {
      test: {
        options: {
          reporter: 'spec',
          clearRequireCache: true
        },
        src: ['test/**/*.js']
      }
    }
  });

  // Load tasks
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-preprocess');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-nw-builder');
  grunt.loadNpmTasks('grunt-mocha-test');

  // Create targets
  grunt.registerTask('setup-prod', ['preprocess', 'requirejs:prod', 'cssmin', 'copy']);
  grunt.registerTask('setup-dev', ['preprocess', 'requirejs:dev', 'cssmin', 'copy']);

  grunt.registerTask('build-prod', ['clean', 'setup-prod', 'nwjs']);
  grunt.registerTask('build-dev', ['clean', 'setup-dev', 'nwjs']);

  grunt.registerTask('test', ['preprocess', 'mochaTest']);

  grunt.registerTask('default', ['build-prod']);
};